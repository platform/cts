# Backported Fixes Test libraries.

This directory contains rules, annotations, and other helpers for writing tests
for Backported Fixes.

TODO: Add link to public docs.

Tests for backported fixes should go in the test suite appropriate to the
affected code and be annotated {@link BackportedFixTest}.
