// Copyright (C) 2017 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_test {
    name: "CtsAlarmManagerTestCases",
    team: "trendy_team_framework_backstage_power",
    defaults: ["cts_defaults"],
    static_libs: [
        "androidx.test.rules",
        "compatibility-device-util-axt",
    ],
    srcs: [
        "src/**/*.java",
        "app/src/**/*.java",
        ":CtsAlarmUtils",
        ":CtsAlarmTestHelperCommon",
    ],
    test_suites: [
        "cts",
        "general-tests",
        "mts-scheduling",
        "mcts-scheduling",
    ],
    platform_apis: true,
    data: [
        ":AlarmTestApp30",
        ":AlarmTestAppWithPolicyPermissionSdk32",
        ":AlarmTestApp",
        ":AlarmTestAppWithUserPermissionSdk32",
    ],
    per_testcase_directory: true,
    min_sdk_version: "29",
}

test_module_config {
    name: "CtsAlarmManagerTestCases_NoLarge",
    base: "CtsAlarmManagerTestCases",
    test_suites: ["general-tests"],
    exclude_annotations: ["androidx.test.filters.LargeTest"],
}
