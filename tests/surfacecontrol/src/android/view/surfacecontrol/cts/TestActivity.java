/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.view.surfacecontrol.cts;

import static android.view.WindowInsets.Type.displayCutout;
import static android.view.WindowInsets.Type.systemBars;
import static android.view.WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_ALWAYS;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.WindowInsetsController;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;

import java.util.concurrent.CountDownLatch;

public class TestActivity extends Activity {
    private static final int sTypeMask = systemBars() | displayCutout();
    private FrameLayout mParentLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mParentLayout = new FrameLayout(this);
        mParentLayout.setBackgroundColor(Color.YELLOW);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);
        setContentView(mParentLayout, layoutParams);

        WindowInsetsController windowInsetsController = getWindow().getInsetsController();
        windowInsetsController.hide(sTypeMask);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.layoutInDisplayCutoutMode = LAYOUT_IN_DISPLAY_CUTOUT_MODE_ALWAYS;
        params.flags |= WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;
        getWindow().setAttributes(params);
        getWindow().setDecorFitsSystemWindows(false);
    }

    public FrameLayout getParentLayout() {
        return mParentLayout;
    }

    /** Run a short, but janky animation. */
    public void startJankyAnimation(final CountDownLatch onEnd) {
        final View view = new View(this);
        view.setBackgroundColor(Color.BLUE);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(100, 100);
        getParentLayout().addView(view, layoutParams);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        boolean[] causedJank = new boolean[] { false };
        anim.addUpdateListener($ -> {
            float progress = anim.getAnimatedFraction();
            if (progress > 0.5 && !causedJank[0]) {
                causedJank[0] = true;
                // Let's miss a deadline!
                android.os.SystemClock.sleep(50);
            }
            view.setTranslationX(progress * 100);
            view.invalidate();
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator anim) {
                getParentLayout().removeView(view);
                onEnd.countDown();
            }
        });
        anim.start();
    }
}
