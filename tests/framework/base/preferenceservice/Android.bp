// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_android_settings_app",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_test {
    name: "CtsSettingsPreferenceServiceTest",
    defaults: ["cts_defaults"],
    libs: ["android.test.runner.stubs.test"],
    static_libs: [
        "aconfig_settingslib_flags_java_lib",
        "androidx.test.rules",
        "compatibility-device-util-axt",
        "ctstestrunner-axt",
        "junit",
        "bedstead",
    ],
    srcs: ["src/**/*.kt"],
    // Tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
    ],
    sdk_version: "test_current",
}
