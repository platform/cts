/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.server.biometrics;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

public class ConfirmDeviceCredentialTestActivity extends Activity {
    private static final String TAG = "ConfirmDeviceCredentialTestActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final KeyguardManager keyguardManager = getSystemService(KeyguardManager.class);

        Intent intent = keyguardManager.createConfirmDeviceCredentialIntent("title", "description");
        if (intent != null) {
            startActivityForResult(intent, 0);
        }
    }
}
