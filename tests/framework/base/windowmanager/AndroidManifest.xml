<?xml version="1.0" encoding="utf-8"?>
<!--
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 -->

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
     xmlns:androidprv="http://schemas.android.com/apk/prv/res/android"
     xmlns:tools="http://schemas.android.com/tools"
     package="android.server.wm.cts"
     android:targetSandboxVersion="2">

    <uses-permission android:name="android.permission.ADD_TRUSTED_DISPLAY"/>
    <uses-permission android:name="android.permission.CAPTURE_VIDEO_OUTPUT"/>
    <uses-permission android:name="android.permission.DETECT_SCREEN_CAPTURE"/>
    <uses-permission android:name="android.permission.DETECT_SCREEN_RECORDING" />
    <uses-permission android:name="android.permission.DISABLE_KEYGUARD"/>
    <uses-permission android:name="android.permission.DUMP"/>
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE_MEDIA_PROJECTION" />
    <uses-permission android:name="android.permission.MANAGE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.QUERY_ALL_PACKAGES"/>
    <uses-permission android:name="android.permission.READ_LOGS"/>
    <uses-permission android:name="android.permission.REQUEST_DELETE_PACKAGES"/>
    <uses-permission android:name="android.permission.STOP_APP_SWITCHES"/>
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_SECURE_SETTINGS"/>

    <application android:label="CtsWindowManagerDeviceTestCases"
                 android:requestLegacyExternalStorage="true"
                 android:enableOnBackInvokedCallback="false"
                 android:testOnly="true">
        <uses-library android:name="android.test.runner"/>
        <uses-library android:name="androidx.window.extensions"
            android:required="false" />

        <activity android:name="android.server.wm.ActivityManagerTestBase$ConfigChangeHandlingActivity"
             android:resizeableActivity="true"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$FirstActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SecondActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$ThirdActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SideActivity"
                  android:taskAffinity="nobody.but.SideActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$TranslucentActivity"
             android:theme="@android:style/Theme.Translucent.NoTitleBar"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SecondTranslucentActivity"
             android:theme="@android:style/Theme.Translucent.NoTitleBar"/>

        <activity android:name="android.server.wm.activity.lifecycle.CallbackTrackingActivity"
             android:configChanges="keyboard|keyboardHidden|navigation"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SecondCallbackTrackingActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$TranslucentCallbackTrackingActivity"
             android:theme="@android:style/Theme.Translucent.NoTitleBar"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$ShowWhenLockedCallbackTrackingActivity"
                  android:configChanges="keyboard|keyboardHidden|navigation" />

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SecondProcessCallbackTrackingActivity"
             android:process=":SecondProcess"
             android:exported="true"/>

        <provider android:name="android.server.wm.activity.lifecycle.EventLog"
             android:authorities="android.server.wm.lifecycle.logprovider"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$LaunchForResultActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$ResultActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$TranslucentResultActivity"
                  android:theme="@android:style/Theme.Dialog"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SingleTopActivity"
             android:launchMode="singleTop"/>

        <activity android:name="android.server.wm.activity.lifecycle.LifecycleConfigChangeHandlingActivity"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$PipActivity"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout"
             android:supportsPictureInPicture="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$AlwaysFocusablePipActivity"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout"
             android:resizeableActivity="false"
             android:supportsPictureInPicture="true"
             androidprv:alwaysFocusable="true"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$SlowActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$NoDisplayActivity"
             android:theme="@android:style/Theme.NoDisplay"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$DifferentAffinityActivity"
             android:taskAffinity="nobody.but.DifferentAffinityActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$TransitionSourceActivity"
             android:theme="@style/window_activity_transitions"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$TransitionDestinationActivity"
             android:theme="@style/window_activity_transitions"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$LaunchForwardResultActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleClientTestBase$TrampolineActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleLegacySplitScreenTests$ShowImeActivity"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityLifecycleTopResumedStateTests$NoRelaunchCallbackTrackingActivity"
            android:resizeableActivity="true"
            android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen|keyboard|keyboardHidden|navigation"/>

        <activity android:name="android.server.wm.multidisplay.MultiDisplayActivityLaunchTests$ImmediateLaunchTestActivity"
             android:allowEmbedded="true"/>

        <activity android:name="android.server.wm.ime.MultiDisplayImeTests$ImeTestActivity"
             android:resizeableActivity="true"
             android:theme="@style/no_starting_window"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.ime.MultiDisplayImeTests$ImeTestActivity2"/>
        <activity android:name="android.server.wm.ime.MultiDisplayImeTests$ImeTestActivityWithBrokenContextWrapper"/>

        <activity android:name="android.server.wm.multidisplay.MultiDisplayClientTests$ClientTestActivity"/>
        <activity android:name="android.server.wm.multidisplay.MultiDisplayClientTests$NoRelaunchActivity"
             android:resizeableActivity="true"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen|keyboard|keyboardHidden|navigation"/>

        <activity android:name="android.server.wm.window.HideOverlayWindowsTest$SystemWindowActivity"
                  android:process=":swa"
                  android:launchMode="singleTop"
                  android:exported="true"/>
        <activity android:name="android.server.wm.window.HideOverlayWindowsTest$InternalSystemWindowActivity"
                  android:process=":iswa"
                  android:exported="true"/>
        <activity android:name="android.server.wm.window.HideOverlayWindowsTest$SystemApplicationOverlayActivity"
                  android:process=":saoa"
                  android:exported="true"/>

        <activity android:name="android.server.wm.keyguard.KeyguardLockedTests$ShowImeAfterLockscreenActivity"
                  android:theme="@style/OptOutEdgeToEdge"
                  android:configChanges="keyboard|keyboardHidden|navigation"/>

        <activity android:name="android.server.wm.keyguard.KeyguardLockedTests$ShowWhenLockedImeActivity"/>

        <activity android:name="android.server.wm.HelperActivities$StandardActivity"
             android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$SecondStandardActivity"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$StandardWithSingleTopActivity"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$SingleTopActivity"
             android:launchMode="singleTop"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$SingleInstanceActivity"
             android:launchMode="singleInstance"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$SingleTaskActivity"
             android:launchMode="singleTask"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$DocumentIntoExistingActivity"
                  android:documentLaunchMode="intoExisting"
                  android:exported="true"/>
        <activity-alias
            android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$DocumentIntoExistingAliasActivity"
            android:targetActivity="android.server.wm.activity.lifecycle.ActivityStarterTests$DocumentIntoExistingActivity"
            android:exported="true">
        </activity-alias>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$TestLaunchingActivity"
             android:taskAffinity="nobody.but.LaunchingActivity"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$LaunchingAndFinishActivity"
             android:taskAffinity="nobody.but.LaunchingActivity"
             android:exported="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$ClearTaskOnLaunchActivity"
                  android:clearTaskOnLaunch="true"/>

        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$FinishOnTaskLaunchActivity"
                  android:finishOnTaskLaunch="true"
                  android:exported="true"/>
        <activity android:name="android.server.wm.activity.lifecycle.ActivityStarterTests$RelinquishTaskIdentityActivity"
                  android:exported="true"
                  android:relinquishTaskIdentity="true"/>

        <provider android:name="android.server.wm.TestJournalProvider"
             android:authorities="android.server.wm.testjournalprovider"
             android:exported="true"/>

        <!--intent tests-->
        <activity android:name="android.server.wm.intent.Activities$RegularActivity"/>
        <activity android:name="android.server.wm.intent.Activities$SingleTopActivity"
             android:launchMode="singleTop"/>
        <activity android:name="android.server.wm.intent.Activities$SingleInstanceActivity"
             android:launchMode="singleInstance"/>
        <activity android:name="android.server.wm.intent.Activities$SingleInstanceActivity2"
             android:launchMode="singleInstance"
             android:taskAffinity=".t1"/>
        <activity android:name="android.server.wm.intent.Activities$SingleTaskActivity"
             android:launchMode="singleTask"/>
        <activity android:name="android.server.wm.intent.Activities$SingleTaskActivity2"
             android:launchMode="singleTask"
             android:taskAffinity=".t1"/>
        <activity android:name="android.server.wm.intent.Activities$SingleInstancePerTaskActivity"
             android:launchMode="singleInstancePerTask"/>
        <activity android:name="android.server.wm.intent.Activities$SingleInstancePerTaskDocumentNeverActivity"
             android:launchMode="singleInstancePerTask"
             android:documentLaunchMode="never"/>
        <activity android:name="android.server.wm.intent.Activities$TaskAffinity1Activity"
             android:allowTaskReparenting="true"
             android:launchMode="standard"
             android:taskAffinity=".t1"/>
        <activity android:name="android.server.wm.intent.Activities$TaskAffinity1Activity2"
             android:allowTaskReparenting="true"
             android:launchMode="standard"
             android:taskAffinity=".t1"/>
        <activity android:name="android.server.wm.intent.Activities$TaskAffinity1SingleTopActivity"
             android:allowTaskReparenting="true"
             android:launchMode="singleTop"
             android:taskAffinity=".t1"/>
        <activity android:name="android.server.wm.intent.Activities$TaskAffinity1RelinquishTaskIdentityActivity"
             android:relinquishTaskIdentity="true"
             android:taskAffinity=".t1"/>
        <activity android:name="android.server.wm.intent.Activities$TaskAffinity2Activity"
             android:allowTaskReparenting="true"
             android:launchMode="standard"
             android:taskAffinity=".t2"/>
        <activity android:name="android.server.wm.intent.Activities$TaskAffinity3Activity"
             android:allowTaskReparenting="true"
             android:launchMode="standard"
             android:taskAffinity=".t3"/>
        <activity android:name="android.server.wm.intent.Activities$ClearTaskOnLaunchActivity"
             android:allowTaskReparenting="true"
             android:clearTaskOnLaunch="true"
             android:launchMode="standard"
             android:taskAffinity=".t2"/>
        <activity android:name="android.server.wm.intent.Activities$DocumentLaunchIntoActivity"
             android:documentLaunchMode="intoExisting"/>
        <activity android:name="android.server.wm.intent.Activities$DocumentLaunchAlwaysActivity"
             android:documentLaunchMode="always"/>
        <activity android:name="android.server.wm.intent.Activities$DocumentLaunchNeverActivity"
             android:documentLaunchMode="never"/>
        <activity android:name="android.server.wm.intent.Activities$NoHistoryActivity"
             android:noHistory="true"/>
        <activity android:name="android.server.wm.intent.Activities$LauncherActivity"
             android:documentLaunchMode="always"
             android:launchMode="singleInstance"/>
        <activity android:name="android.server.wm.intent.Activities$RelinquishTaskIdentityActivity"
             android:relinquishTaskIdentity="true"/>

        <service android:name="android.server.wm.TestLogService"
             android:enabled="true"
             android:exported="true">
        </service>

        <activity android:name="android.server.wm.window.AlertWindowsAppOpsTestsActivity"/>
        <activity android:name="android.server.wm.activity.CloseOnOutsideTestActivity"
                  android:theme="@style/no_starting_window"/>
        <activity android:name="android.server.wm.animations.DialogFrameTestActivity" />
        <activity android:name="android.server.wm.insets.DisplayCutoutTests$TestActivity"
                  android:theme="@style/OptOutEdgeToEdge"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout"
                  android:screenOrientation="nosensor"
                  android:turnScreenOn="true"
                  android:showWhenLocked="true"/>
        <activity android:name="android.server.wm.activity.ConfigurationCallbacksTest$TestActivity"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>

        <activity android:name="android.server.wm.insets.RoundedCornerTests$TestActivity"
                  android:configChanges="orientation|screenSize"
                  android:screenOrientation="nosensor"
                  android:turnScreenOn="true"
                  android:showWhenLocked="true"/>

        <activity android:name="android.server.wm.other.PrivacyIndicatorBoundsTests$TestActivity"
                  android:configChanges="orientation|screenSize"
                  android:screenOrientation="nosensor"
                  android:turnScreenOn="true"
                  android:showWhenLocked="true"/>

        <activity android:name="android.server.wm.insets.WindowInsetsAnimationSynchronicityTests$TestActivity"
             android:turnScreenOn="true"
             android:showWhenLocked="true"/>
        <service android:name="android.server.wm.insets.WindowInsetsAnimationSynchronicityTests$SimpleIme"
             android:label="Simple IME"
             android:permission="android.permission.BIND_INPUT_METHOD"
             android:exported="true">
            <intent-filter>
                <action android:name="android.view.InputMethod"/>
            </intent-filter>
            <meta-data android:name="android.view.im"
                 android:resource="@xml/simple_method"/>
        </service>

        <activity android:name="android.server.wm.KeyEventActivity"
             android:exported="true"
             android:configChanges="orientation|screenLayout|keyboard|keyboardHidden|navigation"
             android:showWhenLocked="true"/>
        <activity android:name="android.server.wm.insets.WindowInsetsPolicyTest$TestActivity"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout"
             android:turnScreenOn="true"
             android:showWhenLocked="true"/>
        <activity android:name="android.server.wm.insets.WindowInsetsPolicyTest$FullscreenTestActivity"/>
        <activity android:name="android.server.wm.insets.WindowInsetsPolicyTest$FullscreenWmFlagsTestActivity"/>
        <activity android:name="android.server.wm.insets.WindowInsetsPolicyTest$ImmersiveFullscreenTestActivity"
             android:documentLaunchMode="always"
             android:theme="@style/no_animation"/>
        <activity android:name="android.server.wm.animations.LayoutTests$TestActivity"
             android:theme="@style/no_animation"/>
        <activity android:name="android.server.wm.animations.LocationOnScreenTests$TestActivity"
             android:theme="@style/no_starting_window"/>
        <activity android:name="android.server.wm.animations.LocationInWindowTests$TestActivity"/>
        <activity android:name="android.server.wm.other.EnsureBarContrastTest$TestActivity"
             android:screenOrientation="locked"
                  android:theme="@style/OptOutEdgeToEdge.NoStartingWindow"/>
        <activity android:name="android.server.wm.input.WindowFocusTests$PrimaryActivity"/>
        <activity android:name="android.server.wm.input.WindowFocusTests$SecondaryActivity"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.input.WindowFocusTests$TertiaryActivity"
            android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.activity.ActivityCaptureCallbackTests$PrimaryActivity"/>
        <activity android:name="android.server.wm.activity.ActivityCaptureCallbackTests$SecondaryActivity"/>
        <activity android:name="android.server.wm.input.WindowFocusTests$LosingFocusActivity"/>
        <activity android:name="android.server.wm.input.WindowFocusTests$AutoEngagePointerCaptureActivity" />
        <activity android:name="android.server.wm.MetricsActivity"
             android:exported="true"
             android:resizeableActivity="true"
             android:supportsPictureInPicture="true"
             android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.window.WindowMetricsActivityTests$MinAspectRatioActivity"
                  android:label="MinAspectRatioActivity"
                  android:minAspectRatio="3.0"
                  android:resizeableActivity="false"/>
        <activity android:name="android.app.Activity"/>
        <activity android:name="android.server.wm.insets.WindowInsetsLayoutTests$TestActivity"/>
        <activity android:name="android.server.wm.insets.WindowInsetsControllerTests$TestActivity"
                  android:theme="@style/no_starting_window"/>
        <activity android:name="android.server.wm.insets.WindowInsetsControllerTests$TestHideOnCreateActivity"
                  android:windowSoftInputMode="adjustPan|stateUnchanged"/>
        <activity android:name="android.server.wm.insets.WindowInsetsControllerTests$TestShowOnCreateActivity"/>

        <activity android:name="android.server.wm.other.DragDropTest$DragDropActivity"
             android:theme="@style/OptOutEdgeToEdge"
             android:screenOrientation="locked"
             android:turnScreenOn="true"
             android:showWhenLocked="true"
             android:label="DragDropActivity"
             android:hardwareAccelerated="true"
             android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST"/>
            </intent-filter>
        </activity>

        <activity android:name="android.server.wm.other.DragDropTest$SoftwareCanvasDragDropActivity"
            android:theme="@style/OptOutEdgeToEdge"
            android:screenOrientation="locked"
            android:turnScreenOn="true"
            android:showWhenLocked="true"
            android:label="DragDropTest$SoftwareCanvasDragDropActivity"
            android:hardwareAccelerated="false"
            android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST"/>
            </intent-filter>
        </activity>

        <activity android:name="android.server.wm.insets.DecorInsetTestsBase$TestActivity"
             android:theme="@style/OptOutEdgeToEdge"
             android:label="DecorInsetTestsBase.TestActivity"
             android:exported="true"/>

        <!-- The test tests insets dispatching. Here disables action bar because it might intercept
             the insets and dispatch modified ones. -->
        <activity android:name="android.server.wm.window.WindowPolicyTests$TestActivity"
                  android:theme="@android:style/Theme.Material.NoActionBar"/>

        <!-- INSETS_DECOUPLED_CONFIGURATION_ENFORCED can only be opted out before creating the
             activity. -->
        <activity android:name="android.server.wm.window.WindowPolicyTests$OptOutEdgeToEdgeActivity"
                  android:theme="@style/OptOutEdgeToEdge"/>

        <activity android:name="android.server.wm.window.WindowCtsActivity"
             android:theme="@style/OptOutEdgeToEdge.NoActionBar"
             android:screenOrientation="locked"
             android:turnScreenOn="true"
             android:showWhenLocked="true"
             android:label="WindowCtsActivity"
             android:configChanges="orientation|screenSize|screenLayout|keyboardHidden"
             android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST"/>
            </intent-filter>
        </activity>

        <activity android:name="android.view.cts.surfacevalidator.ASurfaceControlTestActivity"
                  android:screenOrientation="locked"
                  android:theme="@style/WhiteBackgroundTheme"
                  android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LAUNCHER"/>
            </intent-filter>
        </activity>

        <activity android:name="android.server.wm.input.WindowInputTests$TestActivity"
            android:theme="@style/no_starting_window" />

        <activity android:name="android.server.wm.activity.ActivityRecordInputSinkTestsActivity"
            android:theme="@android:style/Theme.Material.NoActionBar"
            android:exported="true"/>

        <activity android:name="android.server.wm.activity.StartActivityAsUserActivity"
             android:showForAllUsers="true"
             android:directBootAware="true"/>

        <activity android:name="android.server.wm.WindowInsetsAnimationTestBase$TestActivity"
            android:screenOrientation="locked"
            android:theme="@style/OptOutEdgeToEdge.NoActionBar"/>
        <activity android:name="android.server.wm.insets.WindowInsetsAnimationControllerTests$ControllerTestActivity"
            android:theme="@android:style/Theme.Material.NoActionBar" />

        <activity android:name="android.server.wm.insets.ForceRelayoutTestBase$TestActivity"
                  android:exported="true"
                  android:showWhenLocked="true"
                  android:turnScreenOn="true"
                  android:keepScreenOn="true"/>

        <activity android:name="android.server.wm.animations.ActivityTransitionTests$LauncherActivity"
            android:theme="@style/Theme.TranslucentBars"
            android:fitsSystemWindows="true" />

        <activity android:name="android.server.wm.animations.ActivityTransitionTests$TransitionActivity"/>

        <activity android:name="android.server.wm.animations.ActivityTransitionTests$CustomBackgroundTransitionActivity"/>

        <activity android:name="android.server.wm.animations.ActivityTransitionTests$TransitionActivityWithWhiteBackground"
            android:theme="@style/Theme.WhiteBackground"
            android:exported="true"
            android:colorMode="wideColorGamut"/>

        <activity android:name="android.server.wm.animations.ActivityTransitionTests$EdgeExtensionActivity"
            android:theme="@style/Theme.EdgeExtensions"
            android:exported="true"
            android:colorMode="wideColorGamut"
            android:fitsSystemWindows="true" />

        <activity android:name="android.server.wm.animations.ActivityTransitionTests$CustomWindowAnimationActivity"
            android:theme="@style/window_task_animation"
            android:exported="true"/>

        <activity android:name="android.server.wm.input.WindowUntrustedTouchTest$TestActivity"
                  android:exported="true"
                  android:configChanges="screenSize|screenLayout|orientation"
                  android:screenOrientation="nosensor" />

        <activity android:name="android.server.wm.display.DisplayHashManagerTest$TestActivity"
                   android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$ResizeableLeftActivity"
                  android:resizeableActivity="true"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$ResizeableRightActivity"
                  android:resizeableActivity="true"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$ResizeablePortraitActivity"
                  android:resizeableActivity="true"
                  android:screenOrientation="portrait"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$ResponsiveActivity"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$NonResizeablePortraitActivity"
                  android:resizeableActivity="false"
                  android:screenOrientation="portrait"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$NonResizeableLandscapeActivity"
                  android:resizeableActivity="false"
                  android:screenOrientation="landscape"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$NonResizeableNonFixedOrientationActivity"
                  android:resizeableActivity="false"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$NonResizeableAspectRatioActivity"
                  android:resizeableActivity="false"
                  android:screenOrientation="portrait"
                  android:minAspectRatio="1.6"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$NonResizeableLargeAspectRatioActivity"
                  android:resizeableActivity="false"
                  android:screenOrientation="portrait"
                  android:minAspectRatio="4"
                  android:exported="true"/>

        <activity android:name="android.server.wm.HelperActivities$SupportsSizeChangesPortraitActivity"
                  android:resizeableActivity="false"
                  android:screenOrientation="portrait"
                  android:exported="true">
        <meta-data android:name="android.supports_size_changes"
                       android:value="true"/>
        </activity>

        <activity android:name="android.server.wm.HelperActivities$NoPropertyChangeOrientationWhileRelaunchingActivity"
                  android:screenOrientation="landscape"
                  android:exported="true"/>

        <service android:name="android.server.wm.display.WindowContextTests$TestWindowService"
                 android:exported="true"
                 android:enabled="true" />
        <activity android:name="android.server.wm.WindowContextTestActivity"
                  android:exported="true"
                  android:resizeableActivity="true"
                  android:supportsPictureInPicture="true"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.animations.BlurTests$BackgroundActivity"
             android:theme="@style/BackgroundImage"
             android:colorMode="wideColorGamut"
             android:exported="true"/>
        <activity android:name="android.server.wm.animations.BlurTests$BlurActivity"
             android:exported="true"
             android:colorMode="wideColorGamut"
             android:theme="@style/BlurTestTheme"/>
        <activity android:name="android.server.wm.animations.BlurTests$BlurAttributesActivity"
             android:exported="true"
             android:colorMode="wideColorGamut"
             android:theme="@style/BlurryDialog"/>
        <activity android:name="android.server.wm.animations.BlurTests$BadBlurActivity"
             android:exported="true"
             android:colorMode="wideColorGamut"
             android:theme="@style/BadBlurryDialog"/>

        <!-- Overrides the activity declaration in AndroidX test library to remove the starting
             animation. -->
        <activity
            android:name="androidx.test.core.app.InstrumentationActivityInvoker$BootstrapActivity"
            tools:replace="android:theme"
            android:theme="@style/WhiteBackgroundTheme" />
        <activity android:name="android.server.wm.taskfragment.SplitActivityLifecycleTest$ActivityA"
                  android:exported="true"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.taskfragment.SplitActivityLifecycleTest$ActivityB"
                  android:exported="true"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.taskfragment.SplitActivityLifecycleTest$ActivityC"
                  android:exported="true"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"/>
        <activity android:name="android.server.wm.taskfragment.SplitActivityLifecycleTest$TranslucentActivity"
                  android:exported="true"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"
                  android:theme="@android:style/Theme.Translucent.NoTitleBar" />
        <activity android:name="android.server.wm.other.HostActivity"
                  android:exported="true">
               <intent-filter>
                 <action android:name="android.server.wm.other.HostActivity"/>
               </intent-filter>
               <intent-filter>
                 <action android:name="android.intent.action.MAIN"/>
                 <category android:name="android.intent.category.LAUNCHER"/>
               </intent-filter>
        </activity>
        <activity android:name="android.server.wm.other.KeepClearRectsTests$TestActivity"
                  android:exported="true"
                  android:theme="@style/NoInsetsTheme" />
        <activity android:name="android.server.wm.other.KeepClearRectsTests$TranslucentTestActivity"
                  android:exported="true"
                  android:theme="@style/NoInsetsTheme.Translucent" />
        <service android:name="android.server.wm.other.AccessibilityTestService"
            android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE"
            android:exported="true">
            <intent-filter>
                <action android:name="android.accessibilityservice.AccessibilityService"/>
            </intent-filter>
            <meta-data android:name="android.accessibilityservice"
                android:resource="@xml/test_accessibilityservice"/>
        </service>

        <activity android:name="android.server.wm.window.SnapshotTaskTests$TestActivity"
            android:theme="@style/WhiteBackgroundTheme"
            android:exported="true">
        </activity>
        <activity android:name="android.server.wm.backnavigation.BackNavigationActivity"
                  android:enableOnBackInvokedCallback="true"
                  android:exported="true"/>
        <activity android:name="android.server.wm.other.PinnedStackTests$TestActivity"
                  android:exported="true"/>
        <activity android:name="android.server.wm.taskfragment.TaskFragmentTrustedModeTest$TranslucentActivity"
                  android:exported="true"
                  android:configChanges="orientation|screenSize|smallestScreenSize|screenLayout|colorMode|density|touchscreen"
                  android:theme="@android:style/Theme.Translucent.NoTitleBar" />
        <activity android:name="android.server.wm.other.LockTaskModeTests$TestActivity"
                  android:exported="true" />
        <activity android:name="android.server.wm.backnavigation.BackGestureInvokedTest$BackInvokedActivity"
                  android:label="BackInvokedActivity"
                  android:enableOnBackInvokedCallback="true"
                  android:exported="true" />
        <activity android:name="android.server.wm.backnavigation.BackGestureInvokedTest$NewTaskActivity"
                  android:enableOnBackInvokedCallback="true"
                  android:exported="true" />
        <activity android:name="android.server.wm.backnavigation.BackGestureInvokedTest$SecondActivity"
                  android:enableOnBackInvokedCallback="true"
                  android:exported="true" />
        <activity android:name="android.server.wm.backnavigation.BackGestureInvokedTest$ImeTestActivity"
                  android:enableOnBackInvokedCallback="true"
                  android:exported="true"
                  android:windowSoftInputMode="stateVisible"/>
        <activity android:name="android.server.wm.animations.DisplayShapeTests$TestActivity"
                  android:theme="@android:style/Theme.Dialog"
                  android:exported="true"/>

        <activity android:name="android.server.wm.animations.MoveAnimationTests$FloatingActivity"
                  android:exported="true"
                  android:noHistory="true"
                  android:theme="@style/MoveAnimationTestTheme"/>
        <activity android:name="android.server.wm.animations.MoveAnimationTests$FloatingActivity$NoMove"
                  android:exported="true"
                  android:noHistory="true"
                  android:theme="@style/MoveAnimationTestTheme.NoMove"/>

        <activity android:name="android.server.wm.MediaProjectionHelper$MediaProjectionActivity" />
        <service
            android:name="android.server.wm.MediaProjectionHelper$MediaProjectionService"
            android:enabled="true"
            android:foregroundServiceType="mediaProjection" />

        <activity android:name="android.server.wm.window.ScreenRecordingCallbackTests$ScreenRecordingCallbackActivity" />
    </application>

    <instrumentation android:name="androidx.test.runner.AndroidJUnitRunner"
         android:targetPackage="android.server.wm.cts"
         android:label="CTS tests of WindowManager">
    </instrumentation>

</manifest>
