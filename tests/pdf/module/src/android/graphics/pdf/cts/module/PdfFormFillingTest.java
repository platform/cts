/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.graphics.pdf.cts.module;

import static android.graphics.pdf.cts.module.Utils.createRenderer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.graphics.pdf.models.FormEditRecord;
import android.graphics.pdf.models.FormWidgetInfo;
import android.graphics.pdf.models.ListItem;
import android.os.Build;

import androidx.annotation.RawRes;
import androidx.test.filters.SdkSuppress;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

@SdkSuppress(minSdkVersion = Build.VERSION_CODES.VANILLA_ICE_CREAM, codeName = "VanillaIceCream")
@RunWith(AndroidJUnit4.class)
public class PdfFormFillingTest {
    private static final int CLICK_FORM = R.raw.click_form;
    private static final int COMBOBOX_FORM = R.raw.combobox_form;
    private static final int LISTBOX_FORM = R.raw.listbox_form;
    private static final int TEXT_FORM = R.raw.text_form;
    private static final int NOT_FORM = R.raw.two_pages;
    private static final int XFA_FORM = R.raw.xfa_form;
    private static final int XFAF_FORM = R.raw.xfaf_form;

    private Context mContext;

    /**
     * Returns {@code true} if every {@code innerRect} is contained by at least one {@code
     * outerRect}
     */
    private static boolean fullyContain(List<Rect> innerRects, List<Rect> outerRects) {
        outerLoop:
        for (Rect inner : innerRects) {
            for (Rect outer : outerRects) {
                if (outer.contains(inner)) {
                    continue outerLoop;
                }
            }
            return false;
        }
        return true;
    }

    private static FormWidgetInfo makeCheckbox(int widgetIndex, Rect widgetRect, boolean readOnly,
            String textValue, String accessibilityLabel) {
        return new FormWidgetInfo.Builder(FormWidgetInfo.WIDGET_TYPE_CHECKBOX, widgetIndex,
                widgetRect, textValue, accessibilityLabel).setReadOnly(readOnly).build();
    }

    private static FormWidgetInfo makeRadioButton(int widgetIndex, Rect widgetRect,
            boolean readOnly, String textValue, String accessibilityLabel) {
        return new FormWidgetInfo.Builder(FormWidgetInfo.WIDGET_TYPE_RADIOBUTTON, widgetIndex,
                widgetRect, textValue, accessibilityLabel).setReadOnly(readOnly).build();
    }

    private static FormWidgetInfo makeCombobox(int widgetIndex, Rect widgetRect, boolean readOnly,
            String textValue, String accessibilityLabel, boolean editableText, float fontSize,
            List<ListItem> listItems) {
        FormWidgetInfo.Builder builder = new FormWidgetInfo.Builder(
                FormWidgetInfo.WIDGET_TYPE_COMBOBOX, widgetIndex, widgetRect, textValue,
                accessibilityLabel).setReadOnly(readOnly).setEditableText(
                editableText).setListItems(listItems);
        if (fontSize > 0) {
            builder.setFontSize(fontSize);
        }
        return builder.build();
    }

    private static FormWidgetInfo makeListbox(int widgetIndex, Rect widgetRect, boolean readOnly,
            String textValue, String accessibilityLabel, boolean multiSelect,
            List<ListItem> listItems) {
        return new FormWidgetInfo.Builder(FormWidgetInfo.WIDGET_TYPE_LISTBOX, widgetIndex,
                widgetRect, textValue, accessibilityLabel).setReadOnly(readOnly).setMultiSelect(
                multiSelect).setListItems(listItems).build();
    }

    private static FormWidgetInfo makeTextField(int widgetIndex, Rect widgetRect, boolean readOnly,
            String textValue, String accessibilityLabel, boolean editableText,
            boolean multiLineText, int maxLength, float fontSize) {
        FormWidgetInfo.Builder builder = new FormWidgetInfo.Builder(
                FormWidgetInfo.WIDGET_TYPE_TEXTFIELD, widgetIndex, widgetRect, textValue,
                accessibilityLabel).setReadOnly(readOnly).setEditableText(
                editableText).setMultiLineText(multiLineText);
        if (fontSize > 0) {
            builder.setFontSize(fontSize);
        }
        if (maxLength > 0) {
            builder.setMaxLength(maxLength);
        }
        return builder.build();
    }

    @Before
    public void setup() {
        mContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void getFormType_none() throws Exception {
        verifyFormType(NOT_FORM, PdfRenderer.PDF_FORM_TYPE_NONE);
    }

    @Test
    public void getFormType_acro() throws Exception {
        verifyFormType(TEXT_FORM, PdfRenderer.PDF_FORM_TYPE_ACRO_FORM);
    }

    @Test
    public void getFormType_xfa() throws Exception {
        verifyFormType(XFA_FORM, PdfRenderer.PDF_FORM_TYPE_XFA_FULL);
    }

    @Test
    public void getFormType_xfaf() throws Exception {
        verifyFormType(XFAF_FORM, PdfRenderer.PDF_FORM_TYPE_XFA_FOREGROUND);
    }

    // getFormWidgetInfo
    @Test
    public void getFormWidgetInfo_checkbox() throws Exception {
        FormWidgetInfo expected = makeCheckbox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ new Rect(135, 70, 155, 90),
                /* readOnly= */ false,
                /* textValue= */ "false",
                /* accessibilityLabel= */ "checkbox");

        verifyFormWidgetInfo(CLICK_FORM, 0, new Point(145, 80), expected);
    }

    @Test
    public void getFormWidgetInfo_radioButton() throws Exception {
        FormWidgetInfo expected = makeRadioButton(
                /* widgetIndex= */ 5,
                /* widgetRect= */ new Rect(85, 230, 105, 250),
                /* readOnly= */ false,
                /* textValue= */ "false",
                /* accessibilityLabel= */ "");

        verifyFormWidgetInfo(CLICK_FORM, 0, new Point(95, 240), expected);
    }

    @Test
    public void getFormWidgetInfo_readOnlyCheckbox() throws Exception {
        FormWidgetInfo expected = makeCheckbox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ new Rect(135, 30, 155, 50),
                /* readOnly= */ true,
                /* textValue= */ "true",
                /* accessibilityLabel= */ "readOnlyCheckbox");

        verifyFormWidgetInfo(CLICK_FORM, 0, new Point(145, 40), expected);
    }

    @Test
    public void getFormWidgetInfo_readOnlyRadioButton() throws Exception {
        FormWidgetInfo expected = makeRadioButton(
                /* widgetIndex= */ 2,
                /* widgetRect= */ new Rect(85, 180, 105, 200),
                /* readOnly= */ true,
                /* textValue= */ "false",
                /* accessibilityLabel= */ "");

        verifyFormWidgetInfo(CLICK_FORM, 0, new Point(95, 190), expected);
    }

    @Test
    public void getFormWidgetInfo_editableCombobox() throws Exception {
        List<ListItem> expectedChoices = Arrays.asList(
                new ListItem(/* label= */ "Foo", /* selected= */ false),
                new ListItem(/* label= */ "Bar", /* selected= */ false),
                new ListItem(/* label= */ "Qux", /* selected= */ false));
        FormWidgetInfo expected = makeCombobox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ new Rect(100, 220, 200, 250),
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Combo_Editable",
                /* editableText= */ true,
                /* fontSize= */ 12.0f,
                /* listItems= */ expectedChoices);

        verifyFormWidgetInfo(COMBOBOX_FORM, 0, new Point(150, 235), expected);
    }

    @Test
    public void getFormWidgetInfo_unEditableCombobox() throws Exception {
        List<ListItem> expectedChoices = Arrays.asList(
                new ListItem(/* label= */ "Apple", /* selected= */ false),
                new ListItem(/* label= */ "Banana", /* selected= */ true),
                new ListItem(/* label= */ "Cherry", /* selected= */ false),
                new ListItem(/* label= */ "Date", /* selected= */ false),
                new ListItem(/* label= */ "Elderberry", /* selected= */ false),
                new ListItem(/* label= */ "Fig", /* selected= */ false),
                new ListItem(/* label= */ "Guava", /* selected= */ false),
                new ListItem(/* label= */ "Honeydew", /* selected= */ false),
                new ListItem(/* label= */ "Indian Fig", /* selected= */ false),
                new ListItem(/* label= */ "Jackfruit", /* selected= */ false),
                new ListItem(/* label= */ "Kiwi", /* selected= */ false),
                new ListItem(/* label= */ "Lemon", /* selected= */ false),
                new ListItem(/* label= */ "Mango", /* selected= */ false),
                new ListItem(/* label= */ "Nectarine", /* selected= */ false),
                new ListItem(/* label= */ "Orange", /* selected= */ false),
                new ListItem(/* label= */ "Persimmon", /* selected= */ false),
                new ListItem(/* label= */ "Quince", /* selected= */ false),
                new ListItem(/* label= */ "Raspberry", /* selected= */ false),
                new ListItem(/* label= */ "Strawberry", /* selected= */ false),
                new ListItem(/* label= */ "Tamarind", /* selected= */ false),
                new ListItem(/* label= */ "Ugli Fruit", /* selected= */ false),
                new ListItem(/* label= */ "Voavanga", /* selected= */ false),
                new ListItem(/* label= */ "Wolfberry", /* selected= */ false),
                new ListItem(/* label= */ "Xigua", /* selected= */ false),
                new ListItem(/* label= */ "Yangmei", /* selected= */ false),
                new ListItem(/* label= */ "Zucchini", /* selected= */ false));
        FormWidgetInfo expected = makeCombobox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ new Rect(100, 170, 200, 200),
                /* readOnly= */ false,
                /* textValue= */ "Banana",
                /* accessibilityLabel= */ "Combo1",
                /* editableText= */ false,
                /* fontSize= */ 0f,
                /* listItems= */ expectedChoices);

        verifyFormWidgetInfo(COMBOBOX_FORM, 0, new Point(150, 185), expected);
    }

    @Test
    public void getFormWidgetInfo_readOnlyCombobox() throws Exception {
        // Notably, choice options are not populated for read-only Comboboxes
        FormWidgetInfo expected = makeCombobox(
                /* widgetIndex= */ 2,
                /* widgetRect= */ new Rect(100, 70, 200, 100),
                /* readOnly= */ true,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Combo_ReadOnly",
                /* editableText= */ false,
                /* fontSize= */ 0f,
                /* listItems= */ List.of());

        verifyFormWidgetInfo(COMBOBOX_FORM, 0, new Point(150, 85), expected);
    }

    @Test
    public void getFormWidgetInfo_listbox() throws Exception {
        List<ListItem> expectedChoices = Arrays.asList(
                new ListItem(/* label= */ "Alberta", /* selected= */ false),
                new ListItem(/* label= */ "British Columbia", /* selected= */ false),
                new ListItem(/* label= */ "Manitoba", /* selected= */ false),
                new ListItem(/* label= */ "New Brunswick", /* selected= */ false), new ListItem(
                        /* label= */ "Newfoundland and Labrador", /* selected= */ false),
                new ListItem(/* label= */ "Nova Scotia", /* selected= */ false),
                new ListItem(/* label= */ "Ontario", /* selected= */ false), new ListItem(
                        /* label= */ "Prince Edward Island", /* selected= */ false),
                new ListItem(/* label= */ "Quebec", /* selected= */ false),
                new ListItem(/* label= */ "Saskatchewan", /* selected= */ true));
        FormWidgetInfo expected = makeListbox(
                /* widgetIndex= */ 6,
                /* widgetRect= */ new Rect(100, 470, 200, 500),
                /* readOnly= */ false,
                /* textValue= */ "Saskatchewan",
                /* accessibilityLabel= */ "Listbox_SingleSelectLastSelected",
                /* multiSelect= */ false,
                /* listItems= */ expectedChoices);

        verifyFormWidgetInfo(LISTBOX_FORM, 0, new Point(150, 485), expected);
    }

    @Test
    public void getFormWidgetInfo_multiselectListbox() throws Exception {
        List<ListItem> expectedChoices = Arrays.asList(
                new ListItem(/* label= */ "Alberta", /* selected= */ false),
                new ListItem(/* label= */ "British Columbia", /* selected= */ false),
                new ListItem(/* label= */ "Manitoba", /* selected= */ false),
                new ListItem(/* label= */ "New Brunswick", /* selected= */ false), new ListItem(
                        /* label= */ "Newfoundland and Labrador", /* selected= */ false),
                new ListItem(/* label= */ "Nova Scotia", /* selected= */ false),
                new ListItem(/* label= */ "Ontario", /* selected= */ false), new ListItem(
                        /* label= */ "Prince Edward Island", /* selected= */ false),
                new ListItem(/* label= */ "Quebec", /* selected= */ false),
                new ListItem(/* label= */ "Saskatchewan", /* selected= */ true));
        FormWidgetInfo expected = makeListbox(
                /* widgetIndex= */ 6,
                /* widgetRect= */ new Rect(100, 470, 200, 500),
                /* readOnly= */ false,
                /* textValue= */ "Saskatchewan",
                /* accessibilityLabel= */ "Listbox_SingleSelectLastSelected",
                /* multiSelect= */ false,
                /* listItems= */ expectedChoices);

        verifyFormWidgetInfo(LISTBOX_FORM, 0, new Point(150, 485), expected);
    }

    @Test
    public void getFormWidgetInfo_readOnlyListbox() throws Exception {
        // Notably, choice options are not populated for read-only Listboxes
        FormWidgetInfo expected = makeListbox(
                /* widgetIndex= */ 2,
                /* widgetRect= */ new Rect(100, 70, 200, 100),
                /* readOnly= */ true,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Listbox_ReadOnly",
                /* multiSelect= */ false,
                /* listItems= */ List.of());

        verifyFormWidgetInfo(LISTBOX_FORM, 0, new Point(150, 85), expected);
    }

    @Test
    public void getFormWidgetInfo_textField() throws Exception {
        FormWidgetInfo expected = makeTextField(
                /* widgetIndex= */ 0,
                /* widgetRect= */ new Rect(100, 170, 200, 200),
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Text Box",
                /* editableText= */ true,
                /* multiLineText= */ false,
                /* maxLength= */ -1,
                /* fontSize= */ 12.0f);

        verifyFormWidgetInfo(TEXT_FORM, 0, new Point(150, 185), expected);
    }

    @Test
    public void getFormWidgetInfo_charLimitTextField() throws Exception {
        FormWidgetInfo expected = makeTextField(
                /* widgetIndex= */ 2,
                /* widgetRect= */ new Rect(100, 225, 200, 250),
                /* readOnly= */ false,
                /* textValue= */ "Elephant",
                /* accessibilityLabel= */ "CharLimit",
                /* editableText= */ true,
                /* multiLineText= */ false,
                /* maxLength= */ 10,
                /* fontSize= */ 12.0f);

        verifyFormWidgetInfo(TEXT_FORM, 0, new Point(150, 235), expected);
    }

    @Test
    public void getFormWidgetInfo_readOnlyTextField() throws Exception {
        FormWidgetInfo expected = makeTextField(
                /* widgetIndex= */ 1,
                /* widgetRect= */ new Rect(100, 70, 200, 100),
                /* readOnly= */ true,
                /* textValue= */ "",
                /* accessibilityLabel= */ "ReadOnly",
                /* editableText= */ false,
                /* multiLineText= */ false,
                /* maxLength= */ -1,
                /* fontSize= */ 0f);

        verifyFormWidgetInfo(TEXT_FORM, 0, new Point(150, 85), expected);
    }

    @Test
    public void getFormWidgetInfo_noFiltering() throws Exception {
        // Notably, choice options are not populated for read-only Comboboxes
        FormWidgetInfo readOnly = makeCombobox(
                /* widgetIndex= */ 2,
                /* widgetRect= */ new Rect(100, 70, 200, 100),
                /* readOnly= */ true,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Combo_ReadOnly",
                /* editableText= */ false,
                /* fontSize= */ 0f,
                /* listItems= */ List.of());
        List<ListItem> combo1Choices = Arrays.asList(
                new ListItem(/* label= */ "Apple", /* selected= */ false),
                new ListItem(/* label= */ "Banana", /* selected= */ true),
                new ListItem(/* label= */ "Cherry", /* selected= */ false),
                new ListItem(/* label= */ "Date", /* selected= */ false),
                new ListItem(/* label= */ "Elderberry", /* selected= */ false),
                new ListItem(/* label= */ "Fig", /* selected= */ false),
                new ListItem(/* label= */ "Guava", /* selected= */ false),
                new ListItem(/* label= */ "Honeydew", /* selected= */ false),
                new ListItem(/* label= */ "Indian Fig", /* selected= */ false),
                new ListItem(/* label= */ "Jackfruit", /* selected= */ false),
                new ListItem(/* label= */ "Kiwi", /* selected= */ false),
                new ListItem(/* label= */ "Lemon", /* selected= */ false),
                new ListItem(/* label= */ "Mango", /* selected= */ false),
                new ListItem(/* label= */ "Nectarine", /* selected= */ false),
                new ListItem(/* label= */ "Orange", /* selected= */ false),
                new ListItem(/* label= */ "Persimmon", /* selected= */ false),
                new ListItem(/* label= */ "Quince", /* selected= */ false),
                new ListItem(/* label= */ "Raspberry", /* selected= */ false),
                new ListItem(/* label= */ "Strawberry", /* selected= */ false),
                new ListItem(/* label= */ "Tamarind", /* selected= */ false),
                new ListItem(/* label= */ "Ugli Fruit", /* selected= */ false),
                new ListItem(/* label= */ "Voavanga", /* selected= */ false),
                new ListItem(/* label= */ "Wolfberry", /* selected= */ false),
                new ListItem(/* label= */ "Xigua", /* selected= */ false),
                new ListItem(/* label= */ "Yangmei", /* selected= */ false),
                new ListItem(/* label= */ "Zucchini", /* selected= */ false));
        FormWidgetInfo combo1 = makeCombobox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ new Rect(100, 170, 200, 200),
                /* readOnly= */ false,
                /* textValue= */ "Banana",
                /* accessibilityLabel= */ "Combo1",
                /* editableText= */ false,
                /* fontSize= */ 0f,
                /* listItems= */ combo1Choices);
        List<ListItem> editableChoices = Arrays.asList(
                new ListItem(/* label= */ "Foo", /* selected= */ false),
                new ListItem(/* label= */ "Bar", /* selected= */ false),
                new ListItem(/* label= */ "Qux", /* selected= */ false));
        FormWidgetInfo editable = makeCombobox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ new Rect(100, 220, 200, 250),
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Combo_Editable",
                /* editableText= */ true,
                /* fontSize= */ 12.0f,
                /* listItems= */ editableChoices);

        verifyFormWidgetInfos(COMBOBOX_FORM, 0, new int[0],
                Arrays.asList(editable, combo1, readOnly));
    }

    @Test
    public void getFormWidgetInfo_filtering() throws Exception {
        FormWidgetInfo checkbox = makeCheckbox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ new Rect(135, 70, 155, 90),
                /* readOnly= */ false,
                /* textValue= */ "false",
                /* accessibilityLabel= */ "checkbox");
        FormWidgetInfo readOnlyCheckbox = makeCheckbox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ new Rect(135, 30, 155, 50),
                /* readOnly= */ true,
                /* textValue= */ "true",
                /* accessibilityLabel= */ "readOnlyCheckbox");

        verifyFormWidgetInfos(CLICK_FORM, 0, new int[] { FormWidgetInfo.WIDGET_TYPE_CHECKBOX },
                Arrays.asList(readOnlyCheckbox, checkbox));
    }

    @Test
    public void getFormWidgetInfo_invalidIndex() throws Exception {
        try (PdfRenderer renderer = createRenderer(CLICK_FORM, mContext)) {
            try (PdfRenderer.Page page = renderer.openPage(0)) {
                int emptyIndex = 999;
                assertThrows(IllegalArgumentException.class,
                        () -> page.getFormWidgetInfoAtIndex(emptyIndex));
            }
        }
    }

    @Test
    public void getFormWidgetInfo_emptyPoint() throws Exception {
        try (PdfRenderer renderer = createRenderer(CLICK_FORM, mContext)) {
            try (PdfRenderer.Page page = renderer.openPage(0)) {
                Point emptyPos = new Point(0, 0);
                assertThrows(IllegalArgumentException.class,
                        () -> page.getFormWidgetInfoAtPosition(emptyPos.x, emptyPos.y));
            }
        }
    }

    // applyEdit - click type widgets
    @Test
    public void applyEdit_clickOnCheckbox() throws Exception {
        Rect widgetArea = new Rect(135, 70, 155, 90);
        FormWidgetInfo before = makeCheckbox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "false",
                /* accessibilityLabel= */ "checkbox");
        Point clickPoint = new Point(145, 80);
        FormEditRecord click = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setClickPoint(clickPoint).build();
        FormWidgetInfo after = makeCheckbox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "true",
                /* accessibilityLabel= */ "checkbox");

        verifyApplyEdit(CLICK_FORM, 0, clickPoint, before, click, after, List.of(widgetArea));
    }

    @Test
    public void applyEdit_clickOnRadioButton() throws Exception {
        Rect widgetArea = new Rect(85, 230, 105, 250);
        FormWidgetInfo before = makeRadioButton(
                /* widgetIndex= */ 5,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "false",
                /* accessibilityLabel= */ "");
        Point clickPoint = new Point(95, 240);
        FormEditRecord click = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 5).setClickPoint(clickPoint).build();
        FormWidgetInfo after = makeRadioButton(
                /* widgetIndex= */ 5,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "true",
                /* accessibilityLabel= */ "");

        verifyApplyEdit(CLICK_FORM, 0, clickPoint, before, click, after, List.of(widgetArea));
    }

    // applyEdit - combobox
    @Test
    public void applyEdit_setChoiceSelectionOnCombobox() throws Exception {
        Rect comboboxArea = new Rect(100, 220, 200, 250);
        List<ListItem> choicesBefore = Arrays.asList(
                new ListItem(/* label= */ "Foo", /* selected= */ false),
                new ListItem(/* label= */ "Bar", /* selected= */ false),
                new ListItem(/* label= */ "Qux", /* selected= */ false));
        FormWidgetInfo widgetBefore = makeCombobox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ comboboxArea,
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Combo_Editable",
                /* editableText= */ true,
                /* fontSize= */ 12.0f,
                /* listItems= */ choicesBefore);
        FormEditRecord selectBar = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 0).setSelectedIndices(new int[]{1}).build();
        List<ListItem> choicesAfter = Arrays.asList(
                new ListItem(/* label= */ "Foo", /* selected= */ false),
                new ListItem(/* label= */ "Bar", /* selected= */ true),
                new ListItem(/* label= */ "Qux", /* selected= */ false));
        FormWidgetInfo widgetAfter = makeCombobox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ comboboxArea,
                /* readOnly= */ false,
                /* textValue= */ "Bar",
                /* accessibilityLabel= */ "Combo_Editable",
                /* editableText= */ true,
                /* fontSize= */ 12.0f,
                /* listItems= */ choicesAfter);

        verifyApplyEdit(COMBOBOX_FORM, 0, new Point(150, 235), widgetBefore, selectBar, widgetAfter,
                List.of(comboboxArea));
    }

    @Test
    public void applyEdit_setTextOnCombobox() throws Exception {
        Rect comboboxArea = new Rect(100, 220, 200, 250);
        List<ListItem> choicesBefore = Arrays.asList(
                new ListItem(/* label= */ "Foo", /* selected= */ false),
                new ListItem(/* label= */ "Bar", /* selected= */ false),
                new ListItem(/* label= */ "Qux", /* selected= */ false));
        FormWidgetInfo widgetBefore = makeCombobox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ comboboxArea,
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Combo_Editable",
                /* editableText= */ true,
                /* fontSize= */ 12.0f,
                /* listItems= */ choicesBefore);
        FormEditRecord setText = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 0).setText("Gecko tail").build();
        FormWidgetInfo widgetAfter = makeCombobox(
                /* widgetIndex= */ 0,
                /* widgetRect= */ comboboxArea,
                /* readOnly= */ false,
                /* textValue= */ "Gecko tail",
                /* accessibilityLabel= */ "Combo_Editable",
                /* editableText= */ true,
                /* fontSize= */ 12.0f,
                /* listItems= */ choicesBefore);

        verifyApplyEdit(COMBOBOX_FORM, 0, new Point(150, 235), widgetBefore, setText, widgetAfter,
                List.of(comboboxArea));
    }

    // applyEdit - listbox
    @Test
    public void applyEdit_setChoiceSelectionOnListbox() throws Exception {
        Rect widgetArea = new Rect(100, 470, 200, 500);
        List<ListItem> choicesBefore = Arrays.asList(
                new ListItem(/* label= */ "Alberta", /* selected= */ false),
                new ListItem(/* label= */ "British Columbia", /* selected= */ false),
                new ListItem(/* label= */ "Manitoba", /* selected= */ false),
                new ListItem(/* label= */ "New Brunswick", /* selected= */ false), new ListItem(
                        /* label= */ "Newfoundland and Labrador", /* selected= */ false),
                new ListItem(/* label= */ "Nova Scotia", /* selected= */ false),
                new ListItem(/* label= */ "Ontario", /* selected= */ false), new ListItem(
                        /* label= */ "Prince Edward Island", /* selected= */ false),
                new ListItem(/* label= */ "Quebec", /* selected= */ false),
                new ListItem(/* label= */ "Saskatchewan", /* selected= */ true));
        FormWidgetInfo widgetBefore = makeListbox(
                /* widgetIndex= */ 6,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Saskatchewan",
                /* accessibilityLabel= */ "Listbox_SingleSelectLastSelected",
                /* multiSelect= */ false,
                /* listItems= */ choicesBefore);
        FormEditRecord clearSelection = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 6).setSelectedIndices(new int[]{0}).build();
        List<ListItem> choicesAfter = Arrays.asList(
                new ListItem(/* label= */ "Alberta", /* selected= */ true),
                new ListItem(/* label= */ "British Columbia", /* selected= */ false),
                new ListItem(/* label= */ "Manitoba", /* selected= */ false),
                new ListItem(/* label= */ "New Brunswick", /* selected= */ false), new ListItem(
                        /* label= */ "Newfoundland and Labrador", /* selected= */ false),
                new ListItem(/* label= */ "Nova Scotia", /* selected= */ false),
                new ListItem(/* label= */ "Ontario", /* selected= */ false), new ListItem(
                        /* label= */ "Prince Edward Island", /* selected= */ false),
                new ListItem(/* label= */ "Quebec", /* selected= */ false),
                new ListItem(/* label= */ "Saskatchewan", /* selected= */ false));
        FormWidgetInfo widgetAfter = makeListbox(
                /* widgetIndex= */ 6,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Alberta",
                /* accessibilityLabel= */ "Listbox_SingleSelectLastSelected",
                /* multiSelect= */ false,
                /* listItems= */ choicesAfter);

        verifyApplyEdit(LISTBOX_FORM, 0, new Point(150, 485), widgetBefore, clearSelection,
                widgetAfter, List.of(widgetArea));
    }

    @Test
    public void applyEdit_setMultipleChoiceSelectionOnListbox() throws Exception {
        Rect widgetArea = new Rect(100, 170, 200, 200);
        List<ListItem> choicesBefore = Arrays.asList(
                new ListItem(/* label= */ "Apple", /* selected= */ false),
                new ListItem(/* label= */ "Banana", /* selected= */ true),
                new ListItem(/* label= */ "Cherry", /* selected= */ false),
                new ListItem(/* label= */ "Date", /* selected= */ false),
                new ListItem(/* label= */ "Elderberry", /* selected= */ false),
                new ListItem(/* label= */ "Fig", /* selected= */ false),
                new ListItem(/* label= */ "Guava", /* selected= */ false),
                new ListItem(/* label= */ "Honeydew", /* selected= */ false),
                new ListItem(/* label= */ "Indian Fig", /* selected= */ false),
                new ListItem(/* label= */ "Jackfruit", /* selected= */ false),
                new ListItem(/* label= */ "Kiwi", /* selected= */ false),
                new ListItem(/* label= */ "Lemon", /* selected= */ false),
                new ListItem(/* label= */ "Mango", /* selected= */ false),
                new ListItem(/* label= */ "Nectarine", /* selected= */ false),
                new ListItem(/* label= */ "Orange", /* selected= */ false),
                new ListItem(/* label= */ "Persimmon", /* selected= */ false),
                new ListItem(/* label= */ "Quince", /* selected= */ false),
                new ListItem(/* label= */ "Raspberry", /* selected= */ false),
                new ListItem(/* label= */ "Strawberry", /* selected= */ false),
                new ListItem(/* label= */ "Tamarind", /* selected= */ false),
                new ListItem(/* label= */ "Ugli Fruit", /* selected= */ false),
                new ListItem(/* label= */ "Voavanga", /* selected= */ false),
                new ListItem(/* label= */ "Wolfberry", /* selected= */ false),
                new ListItem(/* label= */ "Xigua", /* selected= */ false),
                new ListItem(/* label= */ "Yangmei", /* selected= */ false),
                new ListItem(/* label= */ "Zucchini", /* selected= */ false));
        FormWidgetInfo widgetBefore = makeListbox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Banana",
                /* accessibilityLabel= */ "Listbox_MultiSelect",
                /* multiSelect= */ true,
                /* listItems= */ choicesBefore);
        FormEditRecord selectMultiple = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setSelectedIndices(new int[]{1, 2, 3}).build();
        List<ListItem> choicesAfter = Arrays.asList(
                new ListItem(/* label= */ "Apple", /* selected= */ false),
                new ListItem(/* label= */ "Banana", /* selected= */ true),
                new ListItem(/* label= */ "Cherry", /* selected= */ true),
                new ListItem(/* label= */ "Date", /* selected= */ true),
                new ListItem(/* label= */ "Elderberry", /* selected= */ false),
                new ListItem(/* label= */ "Fig", /* selected= */ false),
                new ListItem(/* label= */ "Guava", /* selected= */ false),
                new ListItem(/* label= */ "Honeydew", /* selected= */ false),
                new ListItem(/* label= */ "Indian Fig", /* selected= */ false),
                new ListItem(/* label= */ "Jackfruit", /* selected= */ false),
                new ListItem(/* label= */ "Kiwi", /* selected= */ false),
                new ListItem(/* label= */ "Lemon", /* selected= */ false),
                new ListItem(/* label= */ "Mango", /* selected= */ false),
                new ListItem(/* label= */ "Nectarine", /* selected= */ false),
                new ListItem(/* label= */ "Orange", /* selected= */ false),
                new ListItem(/* label= */ "Persimmon", /* selected= */ false),
                new ListItem(/* label= */ "Quince", /* selected= */ false),
                new ListItem(/* label= */ "Raspberry", /* selected= */ false),
                new ListItem(/* label= */ "Strawberry", /* selected= */ false),
                new ListItem(/* label= */ "Tamarind", /* selected= */ false),
                new ListItem(/* label= */ "Ugli Fruit", /* selected= */ false),
                new ListItem(/* label= */ "Voavanga", /* selected= */ false),
                new ListItem(/* label= */ "Wolfberry", /* selected= */ false),
                new ListItem(/* label= */ "Xigua", /* selected= */ false),
                new ListItem(/* label= */ "Yangmei", /* selected= */ false),
                new ListItem(/* label= */ "Zucchini", /* selected= */ false));
        FormWidgetInfo widgetAfter = makeListbox(
                /* widgetIndex= */ 1,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Banana",
                /* accessibilityLabel= */ "Listbox_MultiSelect",
                /* multiSelect= */ true,
                /* listItems= */ choicesAfter);

        verifyApplyEdit(LISTBOX_FORM, 0, new Point(150, 185), widgetBefore, selectMultiple,
                widgetAfter, List.of(widgetArea));
    }

    @Test
    public void applyEdit_clearSelectionOnListbox() throws Exception {
        Rect widgetArea = new Rect(100, 470, 200, 500);
        List<ListItem> choicesBefore = Arrays.asList(
                new ListItem(/* label= */ "Alberta", /* selected= */ false),
                new ListItem(/* label= */ "British Columbia", /* selected= */ false),
                new ListItem(/* label= */ "Manitoba", /* selected= */ false),
                new ListItem(/* label= */ "New Brunswick", /* selected= */ false), new ListItem(
                        /* label= */ "Newfoundland and Labrador", /* selected= */ false),
                new ListItem(/* label= */ "Nova Scotia", /* selected= */ false),
                new ListItem(/* label= */ "Ontario", /* selected= */ false), new ListItem(
                        /* label= */ "Prince Edward Island", /* selected= */ false),
                new ListItem(/* label= */ "Quebec", /* selected= */ false),
                new ListItem(/* label= */ "Saskatchewan", /* selected= */ true));
        FormWidgetInfo widgetBefore = makeListbox(
                /* widgetIndex= */ 6,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Saskatchewan",
                /* accessibilityLabel= */ "Listbox_SingleSelectLastSelected",
                /* multiSelect= */ false,
                /* listItems= */ choicesBefore);
        FormEditRecord clearSelection = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 6).setSelectedIndices(new int[0]).build();
        List<ListItem> choicesAfter = Arrays.asList(
                new ListItem(/* label= */ "Alberta", /* selected= */ false),
                new ListItem(/* label= */ "British Columbia", /* selected= */ false),
                new ListItem(/* label= */ "Manitoba", /* selected= */ false),
                new ListItem(/* label= */ "New Brunswick", /* selected= */ false), new ListItem(
                        /* label= */ "Newfoundland and Labrador", /* selected= */ false),
                new ListItem(/* label= */ "Nova Scotia", /* selected= */ false),
                new ListItem(/* label= */ "Ontario", /* selected= */ false), new ListItem(
                        /* label= */ "Prince Edward Island", /* selected= */ false),
                new ListItem(/* label= */ "Quebec", /* selected= */ false),
                new ListItem(/* label= */ "Saskatchewan", /* selected= */ false));
        FormWidgetInfo widgetAfter = makeListbox(
                /* widgetIndex= */ 6,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Listbox_SingleSelectLastSelected",
                /* multiSelect= */ false,
                /* listItems= */ choicesAfter);

        verifyApplyEdit(LISTBOX_FORM, 0, new Point(150, 485), widgetBefore, clearSelection,
                widgetAfter, List.of(widgetArea));
    }

    // applyEdit - text field
    @Test
    public void applyEdit_setTextOnTextField() throws Exception {
        Rect widgetArea = new Rect(100, 170, 200, 200);
        FormWidgetInfo widgetBefore = makeTextField(
                /* widgetIndex= */ 0,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "Text Box",
                /* editableText= */ true,
                /* multiLineText= */ false,
                /* maxLength= */ -1,
                /* fontSize= */ 12.0f);
        FormEditRecord setText = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 0).setText("Gecko tail").build();
        FormWidgetInfo widgetAfter = makeTextField(
                /* widgetIndex= */ 0,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Gecko tail",
                /* accessibilityLabel= */ "Text Box",
                /* editableText= */ true,
                /* multiLineText= */ false,
                /* maxLength= */ -1,
                /* fontSize= */ 12.0f);

        verifyApplyEdit(TEXT_FORM, 0, new Point(150, 185), widgetBefore, setText, widgetAfter,
                List.of(widgetArea));
    }

    @Test
    public void applyEdit_clearTextOnTextField() throws Exception {
        Rect widgetArea = new Rect(100, 225, 200, 250);
        FormWidgetInfo widgetBefore = makeTextField(
                /* widgetIndex= */ 2,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "Elephant",
                /* accessibilityLabel= */ "CharLimit",
                /* editableText= */ true,
                /* multiLineText= */ false,
                /* maxLength= */ 10,
                /* fontSize= */ 12.0f);
        FormEditRecord clearText = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 2).setText("").build();
        FormWidgetInfo widgetAfter = makeTextField(
                /* widgetIndex= */ 2,
                /* widgetRect= */ widgetArea,
                /* readOnly= */ false,
                /* textValue= */ "",
                /* accessibilityLabel= */ "CharLimit",
                /* editableText= */ true,
                /* multiLineText= */ false,
                /* maxLength= */ 10,
                /* fontSize= */ 12.0f);

        verifyApplyEdit(TEXT_FORM, 0, new Point(150, 238), widgetBefore, clearText, widgetAfter,
                List.of(widgetArea));
    }

    // applyEdit edge cases - click type widgets
    @Test
    public void applyEdit_clickOnReadOnlyCheckbox() throws Exception {
        FormEditRecord clickOnROCheckbox = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 0).setClickPoint(new Point(145, 40)).build();

        verifyApplyEditThrowsException(CLICK_FORM, 0, clickOnROCheckbox);
    }

    @Test
    public void applyEdit_clickOnReadOnlyRadioButton() throws Exception {
        FormEditRecord clickOnRORadioButton = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 2).setClickPoint(new Point(95, 190)).build();

        verifyApplyEditThrowsException(CLICK_FORM, 0, clickOnRORadioButton);
    }

    @Test
    public void applyEdit_setTextOnClickTypeWidget() throws Exception {
        FormEditRecord setTextOnCheckbox = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setText("New text").build();

        verifyApplyEditThrowsException(CLICK_FORM, 0, setTextOnCheckbox);
    }

    @Test
    public void applyEdit_setChoiceSelectionOnClickTypeWidget() throws Exception {
        FormEditRecord setChoiceOnCB = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setSelectedIndices(new int[]{1, 2, 3}).build();

        verifyApplyEditThrowsException(CLICK_FORM, 0, setChoiceOnCB);
    }

    @Test
    public void applyEdit_clickOnInvalidPoint() throws Exception {
        FormEditRecord clickOnNothing = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 0).setClickPoint(new Point(0, 0)).build();

        verifyApplyEditThrowsException(CLICK_FORM, 0, clickOnNothing);
    }

    // applyEdit edge cases - combobox
    @Test
    public void applyEdit_setChoiceSelectionOnReadOnlyCombobox() throws Exception {
        FormEditRecord setChoiceOnROCB = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 2).setSelectedIndices(new int[]{1}).build();

        verifyApplyEditThrowsException(COMBOBOX_FORM, 0, setChoiceOnROCB);
    }

    @Test
    public void applyEdit_setInvalidChoiceSelectionOnCombobox() throws Exception {
        FormEditRecord setBadChoice = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setSelectedIndices(new int[]{100, 365, 1436}).build();

        verifyApplyEditThrowsException(COMBOBOX_FORM, 0, setBadChoice);
    }

    @Test
    public void applyEdit_setTextOnReadOnlyCombobox() throws Exception {
        FormEditRecord setTextOnROCB = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 2).setText("new text").build();

        verifyApplyEditThrowsException(COMBOBOX_FORM, 0, setTextOnROCB);
    }

    // applyEdit edge cases - text field

    @Test
    public void applyEdit_setTextOnUneditableCombobox() throws Exception {
        FormEditRecord setTextOnUneditableCB = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setText("new text").build();

        verifyApplyEditThrowsException(COMBOBOX_FORM, 0, setTextOnUneditableCB);
    }

    @Test
    public void applyEdit_clickOnCombobox() throws Exception {
        FormEditRecord clickOnCB = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setClickPoint(new Point(150, 185)).build();

        verifyApplyEditThrowsException(COMBOBOX_FORM, 0, clickOnCB);
    }

    // applyEdit edge cases - listbox
    @Test
    public void applyEdit_setMultipleChoiceSelectionOnSingleSelectionListbox() throws Exception {
        FormEditRecord pickMultipleOnSingleChoice = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 0).setSelectedIndices(new int[]{1, 2}).build();

        verifyApplyEditThrowsException(LISTBOX_FORM, 0, pickMultipleOnSingleChoice);
    }

    @Test
    public void applyEdit_setChoiceSelectionOnReadOnlyListbox() throws Exception {
        FormEditRecord setChoiceOnROLB = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 2).setSelectedIndices(new int[]{1}).build();

        verifyApplyEditThrowsException(LISTBOX_FORM, 0, setChoiceOnROLB);
    }

    @Test
    public void applyEdit_clickOnListbox() throws Exception {
        FormEditRecord clickOnLB = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setClickPoint(new Point(150, 235)).build();

        verifyApplyEditThrowsException(LISTBOX_FORM, 0, clickOnLB);
    }

    @Test
    public void applyEdit_setTextOnListbox() throws Exception {
        FormEditRecord setTextOnLB = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setText("new text").build();

        verifyApplyEditThrowsException(COMBOBOX_FORM, 0, setTextOnLB);
    }

    @Test
    public void applyEdit_setTextOnReadOnlyTextField() throws Exception {
        FormEditRecord setTextOnROTF = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_SET_TEXT,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setText("new text").build();

        verifyApplyEditThrowsException(TEXT_FORM, 0, setTextOnROTF);
    }

    @Test
    public void applyEdit_clickOnTextField() throws Exception {
        FormEditRecord clickOnTF = new FormEditRecord.Builder(FormEditRecord.EDIT_TYPE_CLICK,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setClickPoint(new Point(150, 185)).build();

        verifyApplyEditThrowsException(TEXT_FORM, 0, clickOnTF);
    }

    @Test
    public void applyEdit_setChoiceSelectionOnTextField() throws Exception {
        FormEditRecord setChoiceOnTF = new FormEditRecord.Builder(
                FormEditRecord.EDIT_TYPE_SET_INDICES,
                /* pageNumber= */ 0,
                /* widgetIndex= */ 1).setSelectedIndices(new int[]{1, 2, 3}).build();

        verifyApplyEditThrowsException(TEXT_FORM, 0, setChoiceOnTF);
    }

    private void verifyFormType(@RawRes int docRes, int expectedFormType) throws Exception {
        try (PdfRenderer renderer = createRenderer(docRes, mContext)) {
            int formType = renderer.getPdfFormType();
            assertEquals(expectedFormType, formType);
        }
    }

    private void verifyFormWidgetInfo(@RawRes int docRes, int pageNum, Point position,
            FormWidgetInfo expectedInfo) throws Exception {
        try (PdfRenderer renderer = createRenderer(docRes, mContext)) {
            try (PdfRenderer.Page page = renderer.openPage(pageNum)) {
                // Verify position API
                FormWidgetInfo foundInfo = page.getFormWidgetInfoAtPosition(position.x, position.y);

                assertEquals(expectedInfo, foundInfo);
                // Verify index API
                assertEquals(foundInfo, page.getFormWidgetInfoAtIndex(foundInfo.getWidgetIndex()));
            }
        }
    }

    private void verifyFormWidgetInfos(@RawRes int docRes, int pageNum, int[] widgetTypes,
            List<FormWidgetInfo> expectedInfos) throws Exception {
        try (PdfRenderer renderer = createRenderer(docRes, mContext)) {
            try (PdfRenderer.Page page = renderer.openPage(pageNum)) {
                List<FormWidgetInfo> foundInfos;
                if (widgetTypes.length > 0) {
                    foundInfos = page.getFormWidgetInfos(widgetTypes);
                } else {
                    foundInfos = page.getFormWidgetInfos();
                }

                assertEquals(expectedInfos.size(), foundInfos.size());
                for (int i = 0; i < foundInfos.size(); i++) {
                    FormWidgetInfo expectedInfo = expectedInfos.get(i);
                    assertEquals(expectedInfo, foundInfos.get(i));
                }
            }
        }
    }

    private void verifyApplyEdit(@RawRes int docRes, int pageNum, Point position,
            FormWidgetInfo expectedInfoBefore, FormEditRecord editRecord,
            FormWidgetInfo expectedInfoAfter, List<Rect> expectedInvalidRects) throws Exception {
        try (PdfRenderer renderer = createRenderer(docRes, mContext)) {
            try (PdfRenderer.Page page = renderer.openPage(pageNum)) {
                FormWidgetInfo beforeInfo = page.getFormWidgetInfoAtPosition(position.x,
                        position.y);
                assertEquals(expectedInfoBefore, beforeInfo);

                List<Rect> invalidatedRects = page.applyEdit(editRecord);

                FormWidgetInfo afterInfo = page.getFormWidgetInfoAtPosition(position.x, position.y);
                assertEquals(expectedInfoAfter, afterInfo);
                // A compatible implementation of this API may invalidate a larger area than we
                // expect, but it cannot invalidate a smaller area than we expect.
                assertTrue(fullyContain(
                        /* innerRects= */ expectedInvalidRects,
                        /* outerRects= */ invalidatedRects));
            }
        }
    }

    private void verifyApplyEditThrowsException(@RawRes int docRes, int pageNum,
            FormEditRecord editRecord) throws Exception {
        try (PdfRenderer renderer = createRenderer(docRes, mContext)) {
            try (PdfRenderer.Page page = renderer.openPage(pageNum)) {
                assertThrows(IllegalArgumentException.class, () -> page.applyEdit(editRecord));
            }
        }
    }
}
