<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (C) 2017 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->

<configuration description="Config for CTS Packageinstaller Session test cases">
    <option name="test-suite-tag" value="cts" />
    <option name="config-descriptor:metadata" key="component" value="packagemanager" />
    <option name="config-descriptor:metadata" key="parameter" value="instant_app" />
    <option name="config-descriptor:metadata" key="parameter" value="not_multi_abi" />
    <option name="config-descriptor:metadata" key="parameter" value="secondary_user" />
    <option name="config-descriptor:metadata" key="parameter" value="secondary_user_on_secondary_display" />
    <option name="config-descriptor:metadata" key="parameter" value="no_foldable_states" />
    <option name="config-descriptor:metadata" key="parameter" value="run_on_sdk_sandbox" />

    <target_preparer class="com.android.tradefed.targetprep.RunCommandTargetPreparer">
        <option name="run-command" value="mkdir -p /data/local/tmp/cts/packageinstaller" />
        <option name="teardown-command" value="rm -rf /data/local/tmp/cts"/>
    </target_preparer>

    <!-- disable GPP UI -->
    <target_preparer class="com.android.tradefed.targetprep.DeviceSetup">
        <option name="force-skip-system-props" value="true" />
        <option name="set-global-setting" key="verifier_engprod" value="1" />
        <option name="set-global-setting" key="verifier_verify_adb_installs" value="0" />
        <option name="restore-settings" value="true" />
    </target_preparer>

    <target_preparer class="com.android.tradefed.targetprep.RunCommandTargetPreparer">
        <option name="run-command" value="settings put system pointer_location 1" />
        <option name="run-command" value="settings put system show_touches 1" />
        <option name="teardown-command" value="settings put system pointer_location 0" />
        <option name="teardown-command" value="settings put system show_touches 0" />
    </target_preparer>

    <target_preparer class="com.android.tradefed.targetprep.RunCommandTargetPreparer">
        <option name="run-command" value="pm uninstall android.packageinstaller.emptytestapp.cts" />
        <!-- dismiss all system dialogs before launch test -->
        <option name="run-command" value="am broadcast -a android.intent.action.CLOSE_SYSTEM_DIALOGS" />
        <option name="teardown-command" value="pm uninstall android.packageinstaller.emptytestapp.cts" />
    </target_preparer>

    <target_preparer class="com.android.compatibility.common.tradefed.targetprep.FilePusher">
        <option name="cleanup" value="true" />
        <option name="push" value="CtsEmptyTestApp.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestApp.apk" />
        <option name="push" value="CtsEmptyTestAppV2.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestAppV2.apk" />
        <option name="push" value="CtsEmptyTestApp_pl.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestApp_pl.apk" />
        <option name="push" value="CtsEmptyInstallerApp.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyInstallerApp.apk" />
        <option name="push" value="CtsEmptyTestApp_FullScreenIntent.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestApp_FullScreenIntent.apk" />
        <option name="push" value="CtsEmptyTestApp_AppMetadataInApk.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestApp_AppMetadataInApk.apk" />
        <option name="push" value="CtsEmptyTestApp_AppMetadataInApk_ExceedSizeLimit.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestApp_AppMetadataInApk_ExceedSizeLimit.apk" />
        <option name="push" value="CtsEmptyTestApp_RejectedByVerifier.apk->/data/local/tmp/cts/packageinstaller/CtsEmptyTestApp_RejectedByVerifier.apk" />
        <option name="push" value="CtsSufficientVerifierReject.apk->/data/local/tmp/cts/packageinstaller/CtsSufficientVerifierReject.apk" />
        <option name="push" value="CtsSufficientVerifierReject.apk.idsig->/data/local/tmp/cts/packageinstaller/CtsSufficientVerifierReject.apk.idsig" />
    </target_preparer>

    <target_preparer class="com.android.tradefed.targetprep.suite.SuiteApkInstaller">
        <option name="cleanup-apks" value="true" />
        <option name="test-file-name" value="CtsPackageInstallTestCases.apk" />
        <option name="test-file-name" value="CtsDisabledLauncherActivityApp.apk" />
    </target_preparer>

    <target_preparer class="com.android.tradefed.targetprep.RunCommandTargetPreparer">
        <option name="test-user-token" value="%TEST_USER%"/>
        <option name="run-command" value="appops set --user %TEST_USER% android.packageinstaller.install.cts REQUEST_INSTALL_PACKAGES allow" />
    </target_preparer>

    <target_preparer class="com.android.compatibility.common.tradefed.targetprep.SettingsPreparer">
        <option name="device-setting" value="hide_error_dialogs"/>
        <option name="setting-type" value="global"/>
        <option name="set-value" value="1"/>
    </target_preparer>

    <target_preparer class="com.android.tradefed.targetprep.RunCommandTargetPreparer">
        <option name="run-command" value="pm uninstall com.android.cts.install.lib.testapp.A" />
        <option name="run-command" value="pm uninstall com.android.cts.install.lib.testapp.B" />
        <option name="run-command" value="pm uninstall com.android.cts.install.lib.testapp.S" />
        <option name="run-command" value="pm uninstall com.test.sdk.user" />
        <option name="run-command" value="pm uninstall com.test.sdk1_1" />
        <option name="teardown-command" value="pm uninstall com.android.cts.install.lib.testapp.A" />
        <option name="teardown-command" value="pm uninstall com.android.cts.install.lib.testapp.B" />
        <option name="teardown-command" value="pm uninstall com.android.cts.install.lib.testapp.S" />
        <option name="teardown-command" value="pm uninstall com.test.sdk.user" />
        <option name="teardown-command" value="pm uninstall com.test.sdk1_1" />
        <option name="teardown-command" value="pm uninstall android.packageinstaller.disabledlauncheractivity.cts" />
        <option name="run-command" value="input keyevent KEYCODE_WAKEUP" />
        <option name="run-command" value="wm dismiss-keyguard" />
        <!-- Collapse notifications -->
        <option name="run-command" value="cmd statusbar collapse" />
        <!-- dismiss all system dialogs before launch test -->
        <option name="run-command" value="am broadcast -a android.intent.action.CLOSE_SYSTEM_DIALOGS" />
    </target_preparer>

    <!-- Ensures that screen recording is captured -->
    <target_preparer class="com.android.tradefed.targetprep.RootTargetPreparer">
        <option name="force-root" value="true" />
        <option name="throw-on-error" value="false" />
    </target_preparer>

    <metrics_collector class="com.android.tradefed.device.metric.FilePullerLogCollector">
        <option name="directory-keys" value="/data/user/0/android.packageinstaller.install.cts/files" />
        <option name="collect-on-run-ended-only" value="true" />
    </metrics_collector>

    <test class="com.android.tradefed.testtype.AndroidJUnitTest" >
        <option name="package" value="android.packageinstaller.install.cts" />
        <option name="runtime-hint" value="1m" />
        <option name="exclude-annotation" value="com.android.bedstead.enterprise.annotations.RequireRunOnWorkProfile" />
        <option name="exclude-annotation" value="com.android.bedstead.multiuser.annotations.RequireRunOnSecondaryUser" />
    </test>
</configuration>
