/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.packageinstaller.install.cts

import android.Manifest
import android.app.Activity
import android.content.pm.ApplicationInfo
import android.content.pm.Flags
import android.content.pm.PackageInstaller
import android.content.pm.PackageManager
import android.content.pm.PackageManager.ApplicationInfoFlags
import android.platform.test.annotations.AppModeFull
import android.platform.test.annotations.RequiresFlagsEnabled
import android.platform.test.flag.junit.DeviceFlagsValueProvider
import android.platform.test.rule.ScreenRecordRule.ScreenRecord
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import com.android.compatibility.common.util.SystemUtil
import com.google.common.truth.Truth.assertThat
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.fail
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@AppModeFull(reason = "Instant apps cannot create installer sessions")
@RunWith(AndroidJUnit4::class)
@ScreenRecord
class UpdateOwnershipEnforcementTest : UpdateOwnershipEnforcementTestBase() {

    companion object {
        const val TEST_INSTALLER_APK_PACKAGE_NAME = "android.packageinstaller.emptyinstaller.cts"
        const val TEST_ARCHIVE_APK_NAME = "CtsArchiveTestApp.apk"
        const val TEST_ARCHIVE_APK_PACKAGE_NAME =
            "android.packageinstaller.archive.cts.archiveapp"
        const val TEST_ARCHIVE_INSTALLER_APK_NAME = "CtsArchiveInstallerApp.apk"
        const val TEST_ARCHIVE_INSTALLER_APK_PACKAGE_NAME =
            "android.packageinstaller.archiveinstaller.cts"
        const val BUTTON_UPDATE_ANYWAY_LABEL = "Update anyway"
    }

    private var isUpdateOwnershipEnforcementAvailable: String? = null
    @get:Rule
    val checkFlagsRule = DeviceFlagsValueProvider.createCheckFlagsRule()!!

    /**
     * Checks that we can get default value from isRequestUpdateOwnership.
     */
    @Test
    fun isRequestUpdateOwnership_notSet_returnFalse() {
        val (sessionId, session) = createSession(
            /* installFlags */
            0,
            /* isMultiPackage */
            false,
            /* packageSource */
            null
        )
        val sessionInfo = pi.getSessionInfo(sessionId)
        assertNotNull(sessionInfo)
        assertEquals(false, sessionInfo!!.isRequestUpdateOwnership)
        assertEquals(false, session.isRequestUpdateOwnership)
    }

    /**
     * Checks that we can get correct value from isRequestUpdateOwnership.
     */
    @Test
    fun isRequestUpdateOwnership_set_returnTrue() {
        val (sessionId, session) = createSession(
            INSTALL_REQUEST_UPDATE_OWNERSHIP,
            /* isMultiPackage */
            false,
            /* packageSource */
            null
        )
        val sessionInfo = pi.getSessionInfo(sessionId)
        assertNotNull(sessionInfo)
        assertEquals(true, sessionInfo!!.isRequestUpdateOwnership)
        assertEquals(true, session.isRequestUpdateOwnership)
    }

    /**
     * Checks that we can enforce the update ownership when the first install.
     */
    @Test
    fun setRequestUpdateOwnership_whenInitialInstall_hasUpdateOwner() {
        // First install the test app with enforcing the update ownership.
        startInstallationViaSession(INSTALL_REQUEST_UPDATE_OWNERSHIP)
        clickInstallerUIButton(INSTALL_BUTTON_ID)

        // request should have succeeded
        val result = getInstallSessionResult()
        assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)

        val sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        // This installer should be the update owner
        assertEquals(context.opPackageName, sourceInfo.updateOwnerPackageName)
    }

    /**
     * Checks that we cannot enforce the update ownership when the update.
     */
    @Test
    fun setRequestUpdateOwnership_whenUpdate_hasNoUpdateOwner() {
        // First install the test app without enforcing the update ownership.
        installTestPackage()
        assertInstalled()

        // Try to update the app with using SessionParams.setRequestUpdateOwnership.
        startInstallationViaSession(INSTALL_REQUEST_UPDATE_OWNERSHIP)
        clickInstallerUIButton(INSTALL_BUTTON_ID)

        // request should have succeeded
        val result = getInstallSessionResult()
        assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)

        val sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        // Since we don't allow enabling the update ownership when the update, the update
        // owner should be null.
        assertEquals(null, sourceInfo.updateOwnerPackageName)
    }

    /**
     * Checks that update owner is removed after it is uninstalled.
     */
    @Test
    fun uninstallUpdateOwner_hasNoUpdateOwner() {
        // Install the test apk and assign above test installer as the update owner
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")
        var sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        assertEquals(TEST_INSTALLER_APK_PACKAGE_NAME, sourceInfo.updateOwnerPackageName)

        uninstallPackage(TEST_INSTALLER_APK_PACKAGE_NAME)

        sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        assertEquals(null, sourceInfo.updateOwnerPackageName)
    }

    /**
     * Checks that shell command can enable the update ownership enforcement.
     */
    @Test
    fun installViaShellCommand_enableUpdateOwnership() {
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")

        val sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        assertEquals(TEST_INSTALLER_APK_PACKAGE_NAME, sourceInfo.updateOwnerPackageName)
    }

    /**
     * Checks that an update owner can update the package without user action.
     */
    @Test
    fun updateOwnershipEnforcement_updateByOwner_hasNoUserAction() {
        // Install the test app and enable update ownership enforcement with self package
        installTestPackage("--update-ownership -i " + context.opPackageName)

        try {
            InstrumentationRegistry.getInstrumentation().uiAutomation
                .adoptShellPermissionIdentity(Manifest.permission.INSTALL_PACKAGES)
            startInstallationViaSessionNoPrompt()
            // No need to click installer UI here.

            val result = getInstallSessionResult()
            assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)
            assertInstalled()
        } finally {
            InstrumentationRegistry.getInstrumentation().uiAutomation.dropShellPermissionIdentity()
        }
    }

    /**
     * Checks that an installer needs user action to update a package when
     * it's not the update owner even if it has granted INSTALL_PACKAGES permission.
     */
    @Test
    fun updateOwnershipEnforcement_updateByNonOwner_hasUserAction() {
        // Install the test app and enable update ownership enforcement with another package
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")

        try {
            InstrumentationRegistry.getInstrumentation().uiAutomation
                .adoptShellPermissionIdentity(Manifest.permission.INSTALL_PACKAGES)
            startInstallationViaSession()

            assertInstallerLabelShown()

            // Expecting a prompt to proceed.
            clickInstallerUIButton(INSTALL_BUTTON_ID)

            val result = getInstallSessionResult()
            assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)
            assertInstalled()
        } finally {
            InstrumentationRegistry.getInstrumentation().uiAutomation.dropShellPermissionIdentity()
        }
    }

    /**
     * Checks that an installer needs user action to update a package when
     * it's not the update owner even if it has granted INSTALL_PACKAGES permission.
     * This test simulates sideloading an APK when an installed app has an update owner set.
     */
    @Test
    fun updateOwnershipEnforcement_updateViaIntentByNonOwner_hasUserAction() {
        // Install the test app and enable update ownership enforcement with another package
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")
        assertInstalled()

        try {
            InstrumentationRegistry.getInstrumentation().uiAutomation
                .adoptShellPermissionIdentity(Manifest.permission.INSTALL_PACKAGES)
            val result = startInstallationViaIntent()

            assertInstallerLabelShown()

            // The dialog to confirm update ownership will be shown
            clickInstallerUIButton(INSTALL_BUTTON_ID)

            assertThat(
                result.get(GLOBAL_TIMEOUT, TimeUnit.MILLISECONDS)
            ).isEqualTo(Activity.RESULT_OK)
            assertInstalled()
        } finally {
            InstrumentationRegistry.getInstrumentation().uiAutomation.dropShellPermissionIdentity()
        }
    }

    @Test
    @RequiresFlagsEnabled(Flags.FLAG_ARCHIVING)
    fun updateArchivedAppViaSessionByNonOwner_showConfirmDialog() {
        installPackage(TEST_ARCHIVE_INSTALLER_APK_NAME)
        assertInstalled(TEST_ARCHIVE_INSTALLER_APK_PACKAGE_NAME)

        // Install the archive test app and enable update ownership enforcement with another package
        installPackage(
            TEST_ARCHIVE_APK_NAME,
            "--update-ownership -i $TEST_ARCHIVE_INSTALLER_APK_PACKAGE_NAME"
        )

        val sourceInfo = pm.getInstallSourceInfo(TEST_ARCHIVE_APK_PACKAGE_NAME)
        // This installer should be the update owner
        assertThat(sourceInfo.updateOwnerPackageName).isEqualTo(
            TEST_ARCHIVE_INSTALLER_APK_PACKAGE_NAME
        )

        try {
            assertThat(
                SystemUtil.runShellCommand(
                    String.format("pm archive %s", TEST_ARCHIVE_APK_PACKAGE_NAME)
                )
            ).isEqualTo("Success\n")

            // Check "installed" flag.
            val applicationInfo: ApplicationInfo? = context.packageManager.getPackageInfo(
                TEST_ARCHIVE_APK_PACKAGE_NAME,
                PackageManager.PackageInfoFlags.of(PackageManager.MATCH_ARCHIVED_PACKAGES)
            ).applicationInfo
            assertEquals((applicationInfo!!.flags and ApplicationInfo.FLAG_INSTALLED).toLong(), 0)

            // Check archive state.
            Assert.assertTrue(applicationInfo.isArchived)

            InstrumentationRegistry.getInstrumentation().uiAutomation
                .adoptShellPermissionIdentity(Manifest.permission.INSTALL_PACKAGES)
            startInstallationViaSession(
                /* installFlags */
                0,
                TEST_ARCHIVE_APK_NAME
            )
            val updateAnywayPattern = Pattern.compile(
                BUTTON_UPDATE_ANYWAY_LABEL,
                Pattern.CASE_INSENSITIVE
            )

            // The dialog to confirm update ownership will be shown
            clickInstallerUIButton(By.text(updateAnywayPattern))

            val result = getInstallSessionResult()
            assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)
            assertInstalled(TEST_ARCHIVE_APK_PACKAGE_NAME)
        } finally {
            InstrumentationRegistry.getInstrumentation().uiAutomation
                .dropShellPermissionIdentity()
            uninstallPackage(TEST_ARCHIVE_INSTALLER_APK_PACKAGE_NAME)
            uninstallPackage(TEST_ARCHIVE_APK_PACKAGE_NAME)
        }
    }

    /**
     * Checks that a privileged installer can update the package without user action even if
     * it's not the update owner when the feature flag is turn off.
     */
    @Test
    fun featureDisabled_updateByNonOwner_hasNoUserAction() {
        // Install the test app and enable update ownership enforcement with another package
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")

        setDeviceProperty(PROPERTY_IS_UPDATE_OWNERSHIP_ENFORCEMENT_AVAILABLE, "false")

        try {
            InstrumentationRegistry.getInstrumentation().uiAutomation
                .adoptShellPermissionIdentity(Manifest.permission.INSTALL_PACKAGES)
            startInstallationViaSessionNoPrompt()
            // No need to click installer UI here.

            val result = getInstallSessionResult()
            assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)
            assertInstalled()
        } finally {
            InstrumentationRegistry.getInstrumentation().uiAutomation.dropShellPermissionIdentity()
        }
    }

    /**
     * Checks that the update owner will be cleared once the installer changes.
     */
    @Test
    fun updateOwnershipEnforcement_updateByNonOwner_hasNoUpdateOwner() {
        // Install the test app and enable update ownership enforcement with another package
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")

        startInstallationViaSession(INSTALL_REQUEST_UPDATE_OWNERSHIP)
        clickInstallerUIButton(INSTALL_BUTTON_ID)

        val result = getInstallSessionResult()
        assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)

        val sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        assertEquals(null, sourceInfo.updateOwnerPackageName)
    }

    /**
     * Checks that the update owner will retain if the installer doesn't change.
     */
    @Test
    fun setRequestUpdateOwnership_notRequestWhenUpdate_ownerRetained() {
        // Install the test app and enable update ownership enforcement with another package
        installTestPackage("--update-ownership -i " + context.opPackageName)

        startInstallationViaSession()
        clickInstallerUIButton(INSTALL_BUTTON_ID)

        val result = getInstallSessionResult()
        assertEquals(PackageInstaller.STATUS_SUCCESS, result.status)

        val sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        assertEquals(context.opPackageName, sourceInfo.updateOwnerPackageName)
    }

    /**
     * Checks that the pending user action reason is REASON_UNSPECIFIED when not requesting the
     * update ownership.
     */
    @Test
    fun getPendingUserActionReason_notRequestUpdateOwnership_reasonUnspecified() {
        installTestPackage()
        assertInstalled()

        val (sessionId, session) = createSession(
            /* installFlags */
            0,
            /* isMultiPackage */
            false,
            /* packageSource */
            null
        )
        writeAndCommitSession(TEST_APK_NAME, session)

        // Since SessionInfo will be null once install is complete, we need to get it when prompting
        val sessionInfo = pi.getSessionInfo(sessionId)
        assertNotNull(sessionInfo)
        assertEquals(
                PackageInstaller.REASON_CONFIRM_PACKAGE_CHANGE,
                sessionInfo!!.getPendingUserActionReason()
        )

        clickInstallerUIButton(INSTALL_BUTTON_ID)
        // request should have succeeded
        getInstallSessionResult()
    }

    /**
     * Checks that the pending user action reason is REASON_REMIND_OWNERSHIP when update owner
     * isn't changed.
     */
    @Test
    fun getPendingUserActionReason_notRequestUpdateOwner_reasonRemindOwnership() {
        installTestPackage("--update-ownership -i $TEST_INSTALLER_APK_PACKAGE_NAME")
        assertInstalled()

        val (sessionId, session) = createSession(
            /* installFlags */
            0,
            /* isMultiPackage */
            false,
            /* packageSource */
            null
        )
        writeAndCommitSession(TEST_APK_NAME, session)

        // Since SessionInfo will be null once install is complete, we need to get it when prompting
        val sessionInfo = pi.getSessionInfo(sessionId)
        assertNotNull(sessionInfo)
        assertEquals(
                PackageInstaller.REASON_REMIND_OWNERSHIP,
                sessionInfo!!.getPendingUserActionReason()
        )

        clickInstallerUIButton(INSTALL_BUTTON_ID)
        // request should have succeeded
        getInstallSessionResult()
    }

    /**
     * Checks that we cannot relinquish the update ownership from non-update owner.
     */
    @Test
    fun relinquishUpdateOwnership_notFromUpdateOwner_throwSecurityException() {
        installTestPackage("--update-ownership")
        var sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
        assertEquals(SHELL_PACKAGE_NAME, sourceInfo.updateOwnerPackageName)

        try {
            pm.relinquishUpdateOwnership(TEST_APK_PACKAGE_NAME)
            fail("relinquishUpdateOwnership from non-update owner should throw SecurityException.")
        } catch (e: SecurityException) {
            // Expected behavior
            sourceInfo = pm.getInstallSourceInfo(TEST_APK_PACKAGE_NAME)
            assertEquals(SHELL_PACKAGE_NAME, sourceInfo.updateOwnerPackageName)
        }
    }

    private fun assertInstallerLabelShown() {
        val installerAppInfo =
            pm.getApplicationInfo(context.packageName, ApplicationInfoFlags.of(0))
        val installerAppLabel = pm.getApplicationLabel(installerAppInfo)

        // Wait for a min 3000ms and max 10000ms for UI to become idle
        instrumentation.uiAutomation.waitForIdle(
            (3 * FIND_OBJECT_TIMEOUT),
            (10 * FIND_OBJECT_TIMEOUT)
        )
        assertNotNull(
            "Installer label \"$installerAppLabel\" not shown",
            uiDevice.wait(
                Until.findObject(By.textContains(installerAppLabel.toString())),
                FIND_OBJECT_TIMEOUT
            )
        )
    }
}
