// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    // See: http://go/android-license-faq
    default_applicable_licenses: ["Android-Apache-2.0"],
    default_team: "trendy_team_machine_learning",
}

android_test {
    name: "CtsRsBlasTestCases",
    defaults: ["cts_defaults"],
    // Include both the 32 and 64 bit versions
    compile_multilib: "both",
    static_libs: ["ctstestrunner-axt"],
    libs: ["android.test.base.stubs"],
    jni_libs: ["libbnnmdata_jni"],
    srcs: [
        "src/**/*.java",
        ":CtsRsBlasTestCases-rscript{CtsRsBlasTestCases.srcjar}",
    ],
    resource_zips: [
        ":CtsRsBlasTestCases-rscript{CtsRsBlasTestCases.res.zip}",
    ],
    // Tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
    ],
    // The SDK version is pinned because ScriptC throw an exception starting
    // at API level 35 (see b/297019750).
    sdk_version: "34",
}

genrule {
    name: "CtsRsBlasTestCases-rscript",
    srcs: [
        "src/**/*.rscript",
        "src/**/*.rsh",
        ":rs_script_api",
        ":rs_clang_headers",
    ],
    tools: [
        "llvm-rs-cc",
        "soong_zip",
    ],
    out: [
        "CtsRsBlasTestCases.srcjar",
        "CtsRsBlasTestCases.res.zip",
    ],
    cmd: "for f in $(locations src/**/*.rscript); do " +
        "  $(location llvm-rs-cc) -Wno-error=deprecated-declarations " +
        "  -o $(genDir)/res/raw -p $(genDir)/src " +
        "  -I $$(dirname $$(echo $(locations :rs_script_api) | awk '{ print $$1 }')) " +
        "  -I $$(dirname $$(echo $(locations :rs_clang_headers) | awk '{ print $$1 }')) $${f}; " +
        "done && " +
        "$(location soong_zip) -srcjar -o $(location CtsRsBlasTestCases.srcjar) -C $(genDir)/src -D $(genDir)/src &&" +
        "$(location soong_zip) -o $(location CtsRsBlasTestCases.res.zip) -C $(genDir)/res -D $(genDir)/res",
}
