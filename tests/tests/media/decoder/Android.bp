// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    // See: http://go/android-license-faq
    default_team: "trendy_team_android_media_codec_framework",
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "cts_tests_tests_media_license", // CC-BY
    ],
}

cc_test_library {
    name: "libctsmediadecodertest_jni",
    srcs: [
        "jni/native-media-jni.cpp",
    ],
    shared_libs: [
        "libandroid",
        "libnativehelper_compat_libc++",
        "liblog",
        "libmediandk",
        "libEGL",
    ],
    header_libs: ["liblog_headers"],
    stl: "libc++_static",
    cflags: [
        "-Werror",
        "-Wall",
        "-DEGL_EGLEXT_PROTOTYPES",
    ],
    gtest: false,
    // this test suite will run on sdk 29 as part of MTS, make sure it's compatible
    // (revisit if/when we add features to this library that require newer sdk.
    sdk_version: "29",
}

java_defaults {
    name: "MediaDecoderTestCases_defaults",
    defaults: ["cts_defaults"],
    // include both the 32 and 64 bit versions
    compile_multilib: "both",
    static_libs: [
        "ctstestrunner-axt",
        "ctstestserver",
        "cts-media-common",
    ],
    jni_libs: [
        "libctscodecutils_jni",
        "libctsmediacommon_jni",
        "libctsmediadecodertest_jni",
        "libnativehelper_compat_libc++",
    ],
    resource_dirs: ["res"],
    aaptflags: [
        // Do not compress these files:
        "-0 .vp9",
        "-0 .ts",
        "-0 .heic",
        "-0 .trp",
        "-0 .ota",
        "-0 .mxmf",
    ],
    srcs: [
        "src/**/*.java",
    ],
    // This test uses private APIs
    platform_apis: true,
    jni_uses_sdk_apis: true,
    host_required: ["cts-dynamic-config"],
    min_sdk_version: "29",
    target_sdk_version: "31",
}

android_test {
    name: "CtsMediaDecoderTestCases",
    defaults: ["MediaDecoderTestCases_defaults"],
    test_config: "AndroidTest.xml",
    test_suites: [
        "cts",
        "general-tests",
    ],
}

android_test {
    name: "MctsMediaDecoderTestCases",
    defaults: ["MediaDecoderTestCases_defaults"],
    test_config: "AndroidTest-mcts.xml",
    test_suites: [
        "cts",
        "general-tests",
        "mcts-media",
        "mts-media",
    ],
}

test_module_config {
    name: "CtsMediaDecoderTestCases_cts_decoderrendertest",
    base: "CtsMediaDecoderTestCases",
    test_suites: ["general-tests"],
    include_filters: ["android.media.decoder.cts.DecoderRenderTest"],
}

test_module_config {
    name: "CtsMediaDecoderTestCases_Presubmit_NoDevice",
    base: "CtsMediaDecoderTestCases",
    test_suites: ["general-tests"],
    include_annotations: ["android.platform.test.annotations.Presubmit"],
    exclude_annotations: ["android.platform.test.annotations.RequiresDevice"],
}

test_module_config {
    name: "MctsMediaDecoderTestCases_Presubmit",
    base: "MctsMediaDecoderTestCases",
    test_suites: ["general-tests"],
    include_annotations: ["android.platform.test.annotations.Presubmit"],
}
