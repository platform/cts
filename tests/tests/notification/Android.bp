// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_system_ui_please_use_a_more_specific_subteam_if_possible_",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_test {
    name: "CtsNotificationTestCases",
    defaults: ["cts_defaults"],
    static_libs: [
        "androidx.test.rules",
        "ctstestrunner-axt",
        "androidx.test.rules",
        "junit",
        "permission-test-util-lib",
        "compatibility-device-util-axt",
        "CtsAppTestStubsShared",
        "android.service.notification.flags-aconfig-java",
        "android.app.flags-aconfig-java",
        "flag-junit",
        "notification_flags_lib",
        "bedstead",
    ],
    libs: [
        "android.test.runner.stubs.test",
        "android.test.base.stubs.test",
    ],
    data: [
        ":CtsAppTestStubs",
        ":NotificationApp",
        ":NotificationProvider",
        ":NotificationListener",
        ":NotificationTrampoline",
        ":NotificationTrampolineApi30",
        ":NotificationTrampolineApi32",
        ":NotificationPressure00",
        ":NotificationPressure01",
        ":NotificationPressure02",
        ":NotificationPressure03",
        ":NotificationPressure04",
        ":NotificationPressure05",
        ":NotificationPressure06",
        ":NotificationPressure07",
    ],
    instrumentation_for: "CtsAppTestStubs",
    srcs: [
        "src/**/*.java",
        "src/**/*.kt",
        "NotificationListener/src/com/android/test/notificationlistener/INotificationUriAccessService.aidl",
        "NotificationListener/src/com/android/test/notificationlistener/INLSControlService.aidl",
    ],
    // Tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
    ],
    sdk_version: "test_current",
    target_sdk_version: "current",
    min_sdk_version: "34",
}

test_module_config {
    name: "CtsNotificationTestCases_notification",
    base: "CtsNotificationTestCases",
    test_suites: ["general-tests"],
    exclude_annotations: ["androidx.test.filters.LargeTest"],
}
