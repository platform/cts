/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony.euicc.cts;

import static androidx.test.InstrumentationRegistry.getContext;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Utility to check device has required Telephony feature.
 */
public class EuiccUtil {
    /**
     * Check either device has FEATURE_TELEPHONY_EUICC or not.
     *
     * @return boolean Return true if the device has {@link PackageManager#FEATURE_TELEPHONY_EUICC}
     */
    public static boolean hasEuiccFeature() {
        Context context = getContext();
        if (context == null) return false;

        PackageManager packageManager = context.getPackageManager();
        return packageManager == null ? false :
                packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_EUICC);
    }
}
