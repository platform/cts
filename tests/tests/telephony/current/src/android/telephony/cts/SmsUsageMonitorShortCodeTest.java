/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony.cts;

import static android.telephony.SmsManager.SMS_CATEGORY_FREE_SHORT_CODE;
import static android.telephony.SmsManager.SMS_CATEGORY_NOT_SHORT_CODE;
import static android.telephony.SmsManager.SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE;
import static android.telephony.SmsManager.SMS_CATEGORY_PREMIUM_SHORT_CODE;
import static android.telephony.SmsManager.SMS_CATEGORY_STANDARD_SHORT_CODE;

import static androidx.test.InstrumentationRegistry.getInstrumentation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import android.content.Context;
import android.content.pm.PackageManager;
import android.platform.test.annotations.AppModeNonSdkSandbox;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;

import androidx.test.annotation.UiThreadTest;

import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for SMS short code pattern matching in SmsUsageMonitor.
 */
public class SmsUsageMonitorShortCodeTest {

    private Context mContext;

    private static final class ShortCodeTest {
        final String countryIso;
        final String address;
        final int category;

        ShortCodeTest(String countryIso, String destAddress, int category) {
            this.countryIso = countryIso;
            this.address = destAddress;
            this.category = category;
        }
    }

    /**
     * List of short code test cases.
     */
    private static final ShortCodeTest[] sShortCodeTests = new ShortCodeTest[] {
            new ShortCodeTest("al", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("al", "4321", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("al", "54321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("al", "15191", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("al", "55500", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("al", "55600", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("al", "654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ae", "625315", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ae", "6211", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ae", "6253", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ae", "6568", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ae", "999", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ae", "998", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),

            new ShortCodeTest("am", "112", expectedReturnCode("112")),
            new ShortCodeTest("am", "101", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("am", "102", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("am", "103", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("am", "71522", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("am", "71512", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("am", "71502", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("am", "222", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "1111", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "9999", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "1121", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "1141", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "1161", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "3024", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("am", "715224", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("am", "71523", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),

            new ShortCodeTest("ar", "1912892", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ar", "191287", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ar", "191289", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ar", "39010", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("at", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("at", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("at", "0901234", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("at", "0900666266", SMS_CATEGORY_PREMIUM_SHORT_CODE),

            new ShortCodeTest("au", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("au", "180000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("au", "190000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("au", "1900000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("au", "19000000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("au", "19998882", SMS_CATEGORY_PREMIUM_SHORT_CODE),

            new ShortCodeTest("az", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("az", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "12345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "87744", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "3301", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "3302", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "9012", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "9014", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "9394", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "87744", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "93101", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("az", "123456", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("be", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("be", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("be", "567890", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("be", "8000", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("be", "6566", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("be", "7777", SMS_CATEGORY_PREMIUM_SHORT_CODE),

            new ShortCodeTest("bf", "35696", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("bf", "3681", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bf", "3558", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("bg", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("bg", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("bg", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "12345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "1816", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "1915", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "1916", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "1935", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "18423", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "162055", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("bg", "162054", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bg", "1620551", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("br", "265262", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("br", "2654", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("br", "2652", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("br", "26808", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("br", "26807", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),

            new ShortCodeTest("bw", "166416", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("bw", "16649", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("bw", "16641", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("by", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("by", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("by", "3336", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("by", "5013", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("by", "5014", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("by", "7781", SMS_CATEGORY_PREMIUM_SHORT_CODE),

            new ShortCodeTest("ca", "911", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ca", "+18005551234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ca", "8005551234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ca", "20000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ca", "200000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ca", "2000000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ca", "60999", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ca", "88188", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ca", "24470", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("cd", "444123", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("cd", "444124", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cd", "4441234", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ch", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ch", "123", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ch", "234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ch", "3456", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ch", "98765", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ch", "543", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ch", "83111", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ch", "234567", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ch", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("cl", "484857", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("cl", "4849", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cl", "4848", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("cn", "120", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("cn", "1062503000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("cn", "1065123456", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("cn", "1066335588", SMS_CATEGORY_PREMIUM_SHORT_CODE),

            new ShortCodeTest("co", "4912891", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("co", "491272", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("co", "491289", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("co", "890119", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("cr", "4664537", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("cr", "466458", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cr", "466453", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("cy", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("cy", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("cy", "4321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cy", "54321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cy", "654321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cy", "7510", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cy", "987654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("cz", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("cz", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("cz", "9090150", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cz", "90902", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("cz", "987654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("de", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("de", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("de", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "12345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "8888", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "11111", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "11886", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "22022", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "23300", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "3434", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "34567", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "41414", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "55655", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "66766", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "66777", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "70997", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("de", "77677", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "80888", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "1232286", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("de", "987654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("dk", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("dk", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("dk", "1259", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("dk", "16123", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("dk", "987654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("do", "9128922", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("do", "912898", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("do", "912892", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("do", "912", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("dz", "63071", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("dz", "63073", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("dz", "630711", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ec", "4664534", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ec", "466499", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ec", "466453", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ec", "18512", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ee", "112", expectedReturnCode("112")),
            new ShortCodeTest("ee", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ee", "123", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "1259", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "15330", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "17999", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "17010", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "17013", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "9034567", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ee", "34567890", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("eg", "10020", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("eg", "10021", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("eg", "100158", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("eg", "100023", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("eg", "1000214", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("es", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("es", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("es", "25165", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("es", "27333", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("es", "995399", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("es", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("et", "8527", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("et", "8526",SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("et", "852612", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("fi", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("fi", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("fi", "12345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fi", "123456", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fi", "17159", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fi", "17163", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fi", "0600123", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fi", "070012345", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fi", "987654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("fr", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("fr", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("fr", "34567", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("fr", "45678", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fr", "81185", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("fr", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("fr", "33033", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("fr", "21727", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("gb", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("gb", "999", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("gb", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("gb", "4567", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gb", "45678", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gb", "56789", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gb", "79067", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gb", "80079", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gb", "654321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gb", "7654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ge", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ge", "8765", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "2345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "8012", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "8013", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "8014", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "8889", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "95201", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ge", "95202", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ge", "95203", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ge", "95208", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ge", "952014", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("gh", "37778", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("gh", "3775", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gh", "3777", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("gh", "2333", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("gh", "6061", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("gr", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("gr", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("gr", "54321", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gr", "19567", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gr", "19678", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gr", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("gt", "4664548", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("gt", "466459", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("gt", "466453", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("hu", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hu", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("hu", "012", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hu", "0123", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hu", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hu", "1784", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hu", "2345", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hu", "01234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hu", "012345678", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hu", "0123456789", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hu", "1234567890", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hu", "0691227910", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hu", "2345678901", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hu", "01234567890", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("hn", "4664599", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("hn", "466499", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("hn", "466453", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ie", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ie", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ie", "50123", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ie", "51234", SMS_CATEGORY_STANDARD_SHORT_CODE),
            new ShortCodeTest("ie", "52345", SMS_CATEGORY_STANDARD_SHORT_CODE),
            new ShortCodeTest("ie", "57890", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ie", "67890", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ie", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("il", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("il", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("il", "4422", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("il", "4545", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("il", "987651", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("il", "374771", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("il", "37499", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("il", "37477", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("il", "668191", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("il", "6688", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("il", "6681", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ir", "300086178", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ir", "30008790",SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ir", "700792", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ir", "100016", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ir", "30008360", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("it", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("it", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("it", "4567", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("it", "48000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "45678", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "56789", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "44000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "47000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "48000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "4450000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "4750000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "4850000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "44500", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "47500", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "48500", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "45500", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "49900", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("it", "456789", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("jo", "990661", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("jo", "99006", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("jo", "99066", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("jo", "99390", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("jp", "00050320", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("jp", "00050329",SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("jp", "0000503291", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("kg", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("kg", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kg", "4152", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kg", "4157", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kg", "4449", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kg", "98765", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("kz", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("kz", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kz", "9194", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kz", "7790", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kz", "98765", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ke", "240889", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ke", "24099", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ke", "24088", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ke", "230549", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ke", "23059", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ke", "23054", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("kw", "509761", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("kw", "50979", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("kw", "50976", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("kw", "7112", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("id", "992626", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("id", "99268", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("id", "99265", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("id", "77413", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ls", "320121", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ls", "32013", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ls", "32012", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("lt", "112", expectedReturnCode("112")),
            new ShortCodeTest("lt", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("lt", "123", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lt", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lt", "1381", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lt", "1394", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lt", "1645", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lt", "12345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lt", "123456", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("lu", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("lu", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("lu", "1234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("lu", "12345", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("lu", "64747", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lu", "678901", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("lv", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("lv", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("lv", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lv", "1819", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lv", "1863", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lv", "1874", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("lv", "98765", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ma", "538191", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ma", "53899", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ma", "53819", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ml", "36098", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ml", "36099",SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ml", "360991", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("mn", "4444478", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("mn", "44445", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mn", "44444", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mn", "45678", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mn", "445566", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("mx", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("mx", "2345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mx", "7766", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mx", "23456", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mx", "53035", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mx", "550399", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mx", "550346", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mx", "30303025", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("mx", "3030302", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mx", "3030303", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mx", "81811", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mx", "81818", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("mw", "427611", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("mw", "4279", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mw", "4276", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mw", "4305", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("mw", "4326", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("my", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("my", "1234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("my", "23456", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("my", "32298", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("my", "33776", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("my", "345678", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("my", "66966", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("mz", "171491", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("mz", "1715", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("mz", "1714", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("na", "400059", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("na", "40009", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("na", "40005", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ni", "4664599", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ni", "466499", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ni", "466453", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("nl", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("nl", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("nl", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nl", "4466", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nl", "5040", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nl", "23456", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("no", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("no", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("no", "2201", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("no", "2226", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("no", "2227", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("no", "23456", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("no", "234567", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("nz", "112", expectedReturnCode("112")),
            new ShortCodeTest("nz", "123", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nz", "2345", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nz", "3903", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nz", "8995", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("nz", "23456", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("pe", "3013031", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("pe", "301307", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pe", "301303", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("pk", "9092345", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("pk", "90958", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pk", "9092", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("pk", "909203", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("pk", "909219", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pk", "909201", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("pl", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("pl", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("pl", "7890", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pl", "34567", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pl", "7910", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pl", "74240", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pl", "79866", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pl", "92525", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pl", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ps", "662134", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ps", "6691", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ps", "6681", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("pt", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("pt", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("pt", "61000", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pt", "62345", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pt", "68304", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pt", "69876", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("pt", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("py", "1912891", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("py", "191286", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("py", "191289", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ro", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ro", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("ro", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ro", "1263", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ro", "1288", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ro", "1314", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ro", "1380", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ro", "7890", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ro", "12345", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ru", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ru", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ru", "1161", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ru", "2097", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ru", "3933", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ru", "7781", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ru", "98765", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("rw", "50623", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("rw", "5065", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("rw", "5061", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("se", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("se", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("se", "1234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("se", "72345", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("se", "72999", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("se", "123456", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("se", "87654321", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("sg", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("sg", "1234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("sg", "70000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sg", "79999", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sg", "73800", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sg", "74688", SMS_CATEGORY_STANDARD_SHORT_CODE),
            new ShortCodeTest("sg", "987654", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("si", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("si", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("si", "1234", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("si", "3838", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("si", "72999", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("sn", "212159", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("sn", "21299", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sn", "21215", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("sn", "21098", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("sk", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("sk", "116117", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("sk", "1234", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sk", "6674", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sk", "7604", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sk", "72999", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("sv", "4664599", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("sv", "466499", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("sv", "466453", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("tj", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("tj", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tj", "1161", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tj", "1171", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tj", "4161", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tj", "4449", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tj", "98765", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("tl", "46645", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("tl", "46646", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tl", "466478", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("tn", "85765", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tn", "85799", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("tn", "7720246", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("tn", "772023", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tn", "772024", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("tz", "150467", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("tz", "15049", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tz", "15046", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("tz", "152347", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("tz", "15239", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("tz", "15324", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("tz", "15610", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ua", "112", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ua", "5432", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ua", "4448", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ua", "7094", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ua", "7540", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ua", "98765", SMS_CATEGORY_NOT_SHORT_CODE),

            new ShortCodeTest("ug", "800999", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ug", "8099", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ug", "8009", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("us", "911", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("us", "+18005551234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("us", "8005551234", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("us", "20000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "200000", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "2000000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("us", "20433", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "21472", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "23333", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "99807", SMS_CATEGORY_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "9683999", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("us", "968319", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("us", "96831", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("us", "10907", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("uy", "55003", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("uy", "55002", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("uy", "1912896", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("uy", "191238", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("uy", "191289", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("vn", "90002", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("vn", "1189892", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("vn", "118998", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("vn", "118989", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("vn", "46645", SMS_CATEGORY_FREE_SHORT_CODE),
            new ShortCodeTest("vn", "46634", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),

            new ShortCodeTest("ve", "5383526", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ve", "538358", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ve", "538352", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("ye", "50824", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("ye", "5084", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("ye", "5079", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("za", "330092", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("za", "33001", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("za", "33009", SMS_CATEGORY_FREE_SHORT_CODE),

            new ShortCodeTest("zw", "336791", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("zw", "33642", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("zw", "33679", SMS_CATEGORY_FREE_SHORT_CODE),

            // generic rules for other countries: 5 digits or less considered potential short code
            new ShortCodeTest("zz", "2000000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest("zz", "54321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("zz", "4321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("zz", "321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest("zz", "112", expectedReturnCode("112")),
            new ShortCodeTest(null, "2000000", SMS_CATEGORY_NOT_SHORT_CODE),
            new ShortCodeTest(null, "54321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest(null, "4321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest(null, "321", SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE),
            new ShortCodeTest(null, "112", expectedReturnCode("112")),
    };

    @Before
    public void setUp() throws Exception {
        mContext = getInstrumentation().getTargetContext();
        assumeTrue(mContext.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_TELEPHONY_MESSAGING));
    }

    private static int expectedReturnCode(String address) {
        return PhoneNumberUtils.isEmergencyNumber(address) ?
                SMS_CATEGORY_NOT_SHORT_CODE : SMS_CATEGORY_POSSIBLE_PREMIUM_SHORT_CODE;
    }

    @UiThreadTest
    @Test
    @AppModeNonSdkSandbox(reason = "SDK sandboxes do not have READ_PHONE_STATE permission")
    public void testSmsShortCodeDestination() {
        for (ShortCodeTest test : sShortCodeTests) {
            // It is intended that a short code number in country A may be an emergency number
            // in country B. It is intended that the destination will be changed because of this
            // reason. checkDestination() returns CATEGORY_NOT_SHORT_CODE for emergency numbers.
            if (test.category != SMS_CATEGORY_NOT_SHORT_CODE
                    && PhoneNumberUtils.isEmergencyNumber(test.address)) {
                continue;
            }
            assertEquals("country: " + test.countryIso + " number: " + test.address,
                    test.category, SmsManager.getDefault().checkSmsShortCodeDestination(
                            test.address, test.countryIso));
        }
    }
}
