// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
    default_team: "trendy_team_android_media_solutions_editing",
}

android_test {
    name: "CtsMediaEditingTestCases",
    defaults: ["cts_defaults"],
    // include both the 32 and 64 bit versions
    compile_multilib: "both",
    static_libs: [
        "ctstestrunner-axt",
        "compatibility-device-util-axt",
        "androidx.media3.media3-common",
        "androidx.media3.media3-effect",
        "androidx.media3.media3-exoplayer",
        "androidx.media3.media3-test-utils",
        "androidx.media3.media3-transformer",
        "ctsmediav2common",
    ],
    srcs: [
        "src/**/*.java",
    ],
    libs: [
        "android.test.base.stubs.test",
        "android.test.runner.stubs.test",
    ],
    test_suites: [
        "cts",
        "general-tests",
    ],
    host_required: ["cts-dynamic-config"],
    min_sdk_version: "29",
    target_sdk_version: "31",
    sdk_version: "test_current",
}
