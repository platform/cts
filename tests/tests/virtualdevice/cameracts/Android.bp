// Copyright 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_xr_framework",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_test {
    name: "CtsVirtualDevicesCameraCtsTestCases",
    sdk_version: "test_current",
    defaults: ["cts_defaults"],

    libs: [
        "android.test.runner.stubs.test",
        "android.test.base.stubs.test",
        "CtsCameraTestCases",
    ],

    data: [
        ":CtsCameraTestCases",
    ],

    // Tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
    ],

    per_testcase_directory: true,
}

test_module_config {
    name: "CtsVirtualDevicesCameraCtsTestCases_postsubmit",
    base: "CtsVirtualDevicesCameraCtsTestCases",
    test_suites: ["general-tests"],
    exclude_filters: [
        "android.hardware.camera2.cts.CameraExtensionCharacteristicsTest#testExtensionAvailability",
        "android.hardware.camera2.cts.CameraManagerTest#testCameraManagerGetDeviceIdList[1]",
        "android.hardware.camera2.cts.CaptureRequestTest#testNoiseReductionModeControl[1]",
        "android.hardware.camera2.cts.ConcurrentCameraTest#testMandatoryConcurrentStreamCombination[1]",
        "android.hardware.camera2.cts.ExtendedCameraCharacteristicsTest#testCameraOrientationAlignedWithDevice[1]",
        "android.hardware.camera2.cts.ExtendedCameraCharacteristicsTest#testCameraPerfClassCharacteristics[1]",
        "android.hardware.camera2.cts.ExtendedCameraCharacteristicsTest#testCameraUPerfClassCharacteristics[1]",
        "android.hardware.camera2.cts.StillCaptureTest#testAePrecaptureTriggerCancelJpegCapture[1]",
    ],
}
