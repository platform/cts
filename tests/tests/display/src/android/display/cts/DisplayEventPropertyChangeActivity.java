/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.display.cts;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;

/**
 * A simple activity to manipulate displays to change its properties
 */
public class DisplayEventPropertyChangeActivity extends Activity {
    /**
     * A utility to change the mode of the default display
     */
    public void setModeId(int modeId) {
        runOnUiThread(() -> {
            Window w = getWindow();
            WindowManager.LayoutParams params = w.getAttributes();
            params.preferredDisplayModeId = modeId;
            w.setAttributes(params);
        });
    }
}
