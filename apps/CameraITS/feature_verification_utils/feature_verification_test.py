# Copyright 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for feature_combination_info_pb2."""

import unittest

import feature_combination_info_pb2

class FeatureVerificationUtilsTests(unittest.TestCase):

  #TODO: b/356934871 - add unit test for the library
  def test_give_me_a_name(self):
    pass


if __name__ == "__main__":
  unittest.main()
