<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (C) 2015 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->
    <ScrollView
        android:fitsSystemWindows="@bool/fit_system_windows"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        xmlns:android="http://schemas.android.com/apk/res/android"
        android:id="@+id/scrollView"
        style="@style/RootLayoutPadding">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:orientation="vertical">

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:id="@+id/audio_loopback_headset_port">

                <!-- Features -->
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:textSize="20sp"
                    android:text="@string/ctsv_loopback_features"/>

                <!-- Features, row 1 -->
                <LinearLayout
                    android:orientation="horizontal"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:text="@string/audio_general_proaudio"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_pro_audio"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_lowlatency"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_low_latency"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_mcp"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_mpc"/>
                </LinearLayout>

                <!-- Features, row 2 -->
                <LinearLayout
                    android:orientation="horizontal"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:text="@string/audio_loopback_mmap"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_mmap"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_mmapexclusive"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_mmap_exclusive"/>
                </LinearLayout>

                <!-- Platform -->
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:textSize="20sp"
                    android:text="@string/ctsv_loopback_platform"/>

                <LinearLayout
                    android:orientation="horizontal"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:text="@string/audio_loopback_watch"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_is_watch"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_tv"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_is_TV"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_automobile"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_is_automobile"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_handheld"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_is_handheld"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_marginStart="5sp"
                        android:text="@string/audio_loopback_emulator"/>

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:textStyle="bold"
                        android:id="@+id/audio_loopback_emulator"/>
                </LinearLayout>

                <include layout="@layout/audio_loopback_utilities" />

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:id="@+id/audio_loopback_instructions"
                    android:textSize="20sp"
                    android:gravity="center"/>

                <!-- Speaker/Mic -->
                <LinearLayout
                    android:orientation="vertical"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/audio_loopback_speakermic"
                        android:textSize="18sp"/>

                    <LinearLayout
                        android:orientation="horizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content">

                        <Button
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:text="@string/audio_general_start"
                            android:id="@+id/audio_loopback_speakermicpath_btn" />

                        <TextView
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_gravity="fill"
                            android:text="@string/audio_loopback_speakermicpath_instructions"
                            android:id="@+id/audio_loopback_speakermicpath_info" />
                    </LinearLayout>

                </LinearLayout>

                <!-- Headset Jack -->
                <LinearLayout
                    android:orientation="vertical"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/audio_loopback_headset"
                        android:textSize="18sp"/>

                    <LinearLayout
                        android:orientation="horizontal"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content">

                        <Button
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:text="@string/audio_general_start"
                            android:id="@+id/audio_loopback_headsetpath_btn" />

                        <TextView
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_gravity="fill"
                            android:id="@+id/audio_loopback_headsetpath_info" />
                    </LinearLayout>
            </LinearLayout>

            <!-- USB -->
            <LinearLayout
                android:orientation="vertical"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/audio_loopback_usb"
                    android:textSize="18sp"/>

                <LinearLayout
                    android:orientation="horizontal"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <Button
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@string/audio_general_start"
                        android:id="@+id/audio_loopback_usbpath_btn" />

                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_gravity="fill"
                        android:id="@+id/audio_loopback_usbpath_info" />
                </LinearLayout>
            </LinearLayout>
                <LinearLayout
                    xmlns:android="http://schemas.android.com/apk/res/android"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="vertical">

                    <ProgressBar
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center"
                        android:id="@+id/audio_loopback_progress_bar" />

                    <TextView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@string/audio_loopback_results_text"
                        android:id="@+id/audio_loopback_status_text"
                        android:textSize="20sp" />
                </LinearLayout>
            </LinearLayout>

            <WebView
                android:id="@+id/audio_loopback_results"
                android:layout_width="match_parent"
                android:layout_height="match_parent"/>

            <include layout="@layout/pass_fail_buttons" />
        </LinearLayout>
    </ScrollView>
