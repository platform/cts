//
// Copyright (C) 2010 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    // See: http://go/android-license-faq
    default_applicable_licenses: [
        "cts_apps_CtsVerifier_opencv_license",
        "Android-Apache-2.0",
        "cts_apps_CtsVerifier_fatcow_license",
    ],
}

license {
    name: "cts_apps_CtsVerifier_opencv_license",
    package_name: "opencv",
    license_kinds: [
        "SPDX-license-identifier-BSD",
    ],
    license_text: [
        "libs/opencv-android_LICENSE",
        "res/raw/opencv_library_license",
    ],
}

// See: src/com/android/cts/verifier/features/FeatureSummaryActivity.java
license {
    name: "cts_apps_CtsVerifier_fatcow_license",
    package_name: "fatcow icons",
    license_kinds: [
        "SPDX-license-identifier-CC-BY-3.0",
    ],
    license_text: ["LICENSE_CC_BY"],
}

filegroup {
    name: "CtsVerifierMockVrListenerServiceFiles",
    srcs: ["src/com/android/cts/verifier/vr/MockVrListenerService.java"],
}

android_library {
    name: "CtsVerifierLibT",
    defaults: ["cts_error_prone_rules_tests"],

    srcs: [
        "src/**/*.java",
        "src/**/*.kt",
        "src/**/I*.aidl",
    ],
    exclude_srcs: [
        "src/com/android/cts/verifier/camera/its/*",
    ],

    aidl: {
        include_dirs: ["frameworks/native/aidl/gui"],
    },

    static_libs: [
        "android.media.audio-aconfig-java",
        "android.service.chooser.flags-aconfig-java",
        "android-ex-camera2",
        "compatibility-common-util-devicesidelib",
        "cts-sensors-tests",
        "cts-camera-performance-tests",
        "ctstestrunner-axt",
        "apache-commons-math",
        "androidplot",
        "ctsverifier-opencv",
        "core-tests-support",
        "androidx.appcompat_appcompat",
        "androidx.fragment_fragment",
        "androidx.preference_preference",
        "mockito-target-minus-junit4",
        "mockwebserver",
        "compatibility-device-util-axt",
        "platform-test-annotations",
        "cts-security-test-support-library",
        "cts-midi-lib",
        "cbor-java",
        "CtsCameraUtils",
        "androidx.legacy_legacy-support-v4",
        "CtsForceStopHelper-constants",
        "ctsmediautil",
        "DpmWrapper",
        "MediaPerformanceClassCommon",
        "mediapc-requirements",
        "com_android_wm_shell_flags_lib",
        "its-service",
    ],

    libs: [
        "telephony-common",
        "android.test.runner.stubs",
        "android.test.base.stubs",
        "android.test.mock.stubs",
        "android.car-test-stubs",
        "voip-common",
        "truth",
    ],

    platform_apis: true,

    optimize: {
        proguard_flags_files: ["proguard.flags"],
    },

    dex_preopt: {
        enabled: false,
    },
}

android_test {
    name: "CtsVerifier",
    static_libs: [
        "CtsVerifierLibT",
    ],

    compile_multilib: "both",

    additional_manifests: ["AndroidManifest-verifierConfig.xml"],

    jni_libs: [
        "libctsverifier_jni",
        "libctsnativemidi_jni",
        "libaudioloopback_jni",
        "libmegaaudio_jni",
    ],
}

// opencv library
java_import {
    name: "ctsverifier-opencv",
    jars: ["libs/opencv3-android.jar"],
}

// Build CTS verifier framework as a library.
android_library {
    name: "cts-verifier-framework",

    sdk_version: "test_current",
    min_sdk_version: "29",
    resource_dirs: ["res"],
    srcs: [
        "src/com/android/cts/verifier/*.java",
        "src/**/I*.aidl",
    ],

    static_libs: [
        "androidx.legacy_legacy-support-v4",
        "compatibility-common-util-devicesidelib",
        "compatibility-device-util-axt",
    ],
}

// Build ItsService as a library
android_library {
    name: "its-service",

    sdk_version: "test_current",
    min_sdk_version: "34",
    resource_dirs: ["res"],
    srcs: [
        "src/com/android/cts/verifier/camera/its/*.java",
        "src/com/android/cts/verifier/camera/performance/*.java",
    ],

    static_libs: [
        "android-ex-camera2",
        "cts-camera-performance-tests",
        "cts-verifier-framework",
        "ctstestrunner-axt",
        "androidx.appcompat_appcompat",
        "CtsCameraUtils",
        "MediaPerformanceClassCommon",
        "mediapc-requirements",
    ],
}

filegroup {
    name: "pre_installed_apps",
    device_first_srcs: [
        ":CtsDefaultNotesApp",
        ":CtsDeviceControlsApp",
        ":CtsEmptyDeviceAdmin",
        ":CtsEmptyDeviceOwner",
        ":CtsPermissionApp",
        ":CtsForceStopHelper",
        ":NotificationBot",
        ":CrossProfileTestApp",
        ":CtsTtsEngineSelectorTestHelper",
        ":CtsTtsEngineSelectorTestHelper2",
    ],
}

// Apps to be installed as Instant App using adb install --instant
filegroup {
    name: "pre_installed_instant_app",
    device_first_srcs: [
        ":CtsVerifierInstantApp",
    ],
}

filegroup {
    name: "other_required_apps",
    device_first_srcs: [
        ":CtsCarWatchdogCompanionApp",
        ":CtsTileServiceApp",
        ":CtsVerifierUSBCompanion",
        ":CtsVpnFirewallAppApi23",
        ":CtsVpnFirewallAppApi24",
        ":CtsVpnFirewallAppNotAlwaysOn",
        ":jetpack-camera-app",
    ],
}

filegroup {
    name: "cts_apps_to_include",
    srcs: [
        ":pre_installed_apps",
        ":pre_installed_instant_app",
        ":other_required_apps",
    ],
}

//
// Creates a "cts-verifier" directory that will contain:
//
// 1. Out directory with a "android-cts-verifier" containing the CTS Verifier
//    and other binaries it needs.
//
// 2. Zipped version of the android-cts-verifier directory to be included with
//    the build distribution.
//
genrule {
    name: "android-cts-verifier",
    srcs: [
        ":cts_apps_to_include",
        ":camera-its",
        ":camera-webcam-test",
    ],
    device_common_srcs: [
        ":android-cts-verifier-notice",
        ":CtsVerifier",
        ":multidevice-test",
    ],
    tools: [
        "soong_zip",
        "merge_zips",
    ],
    out: ["android-cts-verifier.zip"],
    cmd: "echo $(locations :cts_apps_to_include) $(location :CtsVerifier) $(location :android-cts-verifier-notice) > $(genDir)/list &&" +
        " $(location soong_zip) -o $(genDir)/cts-verifier.zip -j -P android-cts-verifier -l $(genDir)/list &&" +
        " $(location merge_zips) $(out) $(genDir)/cts-verifier.zip $(location :camera-its) $(location :camera-webcam-test) $(location :multidevice-test)",
    dists: [
        {
            targets: ["cts"],
        },
    ],
}

gen_notice {
    name: "android-cts-verifier-notice",
    for: ["android-cts-verifier"],
    stem: "NOTICE",
    suffix: ".txt",
}

filegroup {
    name: "android-cts-verifier-for-make",
    srcs: [":android-cts-verifier"],
    export_to_make_var: "SOONG_ANDROID_CTS_VERIFIER_ZIP",
}

genrule {
    name: "android-cts-verifier-app-list",
    srcs: [
        ":cts_apps_to_include",
        ":multidevice-test-app-list",
    ],
    device_common_srcs: [
        ":CtsVerifier",
    ],
    out: ["android-cts-verifier-app-list.txt"],
    cmd: "echo $(locations :cts_apps_to_include) $(location :CtsVerifier) $(location :multidevice-test-app-list) > $(out)",
}

filegroup {
    name: "android-cts-verifier-app-list-for-make",
    srcs: [":android-cts-verifier-app-list"],
    export_to_make_var: "SOONG_ANDROID_CTS_VERIFIER_APP_LIST",
}
