/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.cts;

import static com.android.sts.common.CommandUtil.runAndCheck;

import static com.google.common.truth.TruthJUnit.assume;

import android.platform.test.annotations.AsbSecurityTest;

import com.android.sts.common.tradefed.testtype.NonRootSecurityTestCase;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import com.android.tradefed.testtype.junit4.DeviceTestRunOptions;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class CVE_2024_34738 extends NonRootSecurityTestCase {

    @AsbSecurityTest(cveBugId = 336323279)
    @Test
    public void testPocCVE_2024_34738() {
        try {
            // Install test-app
            installPackage("CVE-2024-34738.apk");

            ITestDevice device = getDevice();
            final String pkgOps = "ACCESS_RESTRICTED_SETTINGS";
            final String testPackage = "android.security.cts.CVE_2024_34738";

            // Set the ACCESS_RESTRICTED_SETTINGS operation mode for the testPackage to 'deny'
            runAndCheck(device, String.format("appops set %s %s deny", testPackage, pkgOps));
            String output =
                    runAndCheck(device, String.format("appops get %s %s", testPackage, pkgOps))
                            .getStdout();
            assume().withMessage(
                            "AppOps mode for package: "
                                    + testPackage
                                    + "and Operation: "
                                    + pkgOps
                                    + " is not set to 'deny'")
                    .that(output.contains("deny"))
                    .isTrue();

            // run the DeviceTest
            runDeviceTests(new DeviceTestRunOptions(testPackage));
        } catch (Exception e) {
            assume().that(e).isNull();
        }
    }
}
