// Copyright (C) 2017 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package {
    default_team: "trendy_team_framework_android_packages",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_defaults {
    name: "dynamic_mime_cts_defaults",
    defaults: ["cts_defaults"],

    // Tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
    ],
    libs: [
        "cts-tradefed",
        "tradefed",
        "compatibility-host-util",
        "truth",
    ],
    device_common_data: [
        ":CtsDynamicMimeUpdateAppFirstGroup",
        ":CtsDynamicMimeUpdateAppSecondGroup",
        ":CtsDynamicMimeUpdateAppBothGroups",
        ":CtsDynamicMimePreferredApp",
        ":CtsDynamicMimeHelperApp",
        ":CtsDynamicMimeTestApp",
    ],
    per_testcase_directory: true,
}

java_test_host {
    name: "CtsDynamicMimeHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/TestCases.java",
    ],
}

java_test_host {
    name: "CtsDynamicMimeChangedGroupAppUpdateHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/ChangedGroupsAppUpdateTestCases.java",
    ],
    test_config: "ChangedGroupsAppUpdateTestCases.xml",
}

java_test_host {
    name: "CtsDynamicMimePreferredActivitiesHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/PreferredActivitiesTestCases.java",
    ],
    test_config: "PreferredActivitiesTests.xml",
}

java_test_host {
    name: "CtsDynamicMimeSingleAppRebootHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/RebootTestCaseBase.java",
        "src/**/SingleAppRebootTestCases.java",
    ],
    test_config: "SingleAppRebootTestCases.xml",
}

java_test_host {
    name: "CtsDynamicMimeRemoveRebootHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/RebootTestCaseBase.java",
        "src/**/RemoveRebootTestCases.java",
    ],
    test_config: "RemoveRebootTestCases.xml",
}

java_test_host {
    name: "CtsDynamicMimeSingleAppGroupRebootHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/RebootTestCaseBase.java",
        "src/**/SingleAppGroupRebootTestCases.java",
    ],
    test_config: "SingleAppGroupRebootTestCases.xml",
}

java_test_host {
    name: "CtsDynamicMimeIndependentGroupRebootHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/RebootTestCaseBase.java",
        "src/**/IndependentGroupRebootTestCases.java",
    ],
    test_config: "IndependentGroupRebootTestCases.xml",
}

java_test_host {
    name: "CtsDynamicMimeComplexFilterRebootHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/RebootTestCaseBase.java",
        "src/**/ComplexFilterRebootTestCases.java",
    ],
    test_config: "ComplexFilterRebootTestCases.xml",
}

java_test_host {
    name: "CtsDynamicMimeComplexFilterClearGroupRebootHostTestCases",
    defaults: ["dynamic_mime_cts_defaults"],

    srcs: [
        "src/**/RebootTestCaseBase.java",
        "src/**/ComplexFilterClearGroupRebootTestCases.java",
    ],
    test_config: "ComplexFilterClearGroupRebootTestCases.xml",
}
