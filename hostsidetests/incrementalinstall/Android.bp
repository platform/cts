// Copyright 2020, The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_test_host {
    name: "CtsIncrementalInstallHostTestCases",
    team: "trendy_team_incremental",
    srcs: ["src/**/*.java"],
    libs: [
        "compatibility-host-util",
        "cts-tradefed",
        "tradefed",
        "guava",
    ],
    static_libs: [
        "incremental-install-common-host-lib",
    ],
    test_suites: [
        "cts",
        "general-tests",
    ],
    device_common_data: [
        ":IncrementalTestApp",
        ":IncrementalTestAppUncompressed",
        ":IncrementalTestApp2_v1",
        ":IncrementalTestApp2_v2",
        ":IncrementalTestAppDynamicAsset",
        ":IncrementalTestAppDynamicCode",
        ":IncrementalTestAppCompressedNativeLib",
        ":IncrementalTestAppUncompressedNativeLib",
        ":IncrementalTestAppValidator",
    ],
    per_testcase_directory: true,
}

test_module_config_host {
    name: "CtsIncrementalInstallHostTestCases_incrementalinstall_cts",
    base: "CtsIncrementalInstallHostTestCases",
    test_suites: ["general-tests"],
    include_filters: ["android.incrementalinstall.cts"],
    exclude_annotations: [
        "androidx.test.filters.LargeTest",
        "android.platform.test.annotations.LargeTest",
    ],
}

test_module_config_host {
    name: "CtsIncrementalInstallHostTestCases_cts_incrementalfeaturetest",
    base: "CtsIncrementalInstallHostTestCases",
    test_suites: ["general-tests"],
    include_filters: ["android.incrementalinstall.cts.IncrementalFeatureTest"],
}

test_module_config_host {
    name: "CtsIncrementalInstallHostTestCases_PlatinumTest",
    base: "CtsIncrementalInstallHostTestCases",
    test_suites: ["general-tests"],
    include_annotations: ["android.platform.test.annotations.PlatinumTest"],
}
