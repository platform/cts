// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_framework_backstage_power",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_test_host {
    name: "CtsHostsideNetworkPolicyTests",
    defaults: ["cts_defaults"],
    // Only compile source java files in this apk.
    srcs: [
        "src/**/*.java",
        ":ArgumentConstants",
    ],
    libs: [
        "cts-tradefed",
        "tradefed",
    ],
    static_libs: [
        "modules-utils-build-testing",
        "flag-junit-host",
        "net_flags_host_lib",
    ],
    // Tag this module as a cts test artifact
    test_suites: [
        "cts",
        "general-tests",
        "sts",
    ],
    device_common_data: [
        ":CtsHostsideNetworkPolicyTestsApp",
        ":CtsHostsideNetworkPolicyTestsApp2",
    ],
    per_testcase_directory: true,
}

test_module_config_host {
    name: "CtsHostsideNetworkPolicyTests_com_android_server_job",
    base: "CtsHostsideNetworkPolicyTests",
    test_suites: ["general-tests"],
    include_filters: [
        "com.android.cts.netpolicy.HostsideRestrictBackgroundNetworkTests#testNonMeteredNetworkAccess_expeditedJob",
        "com.android.cts.netpolicy.HostsideRestrictBackgroundNetworkTests#testMeteredNetworkAccess_expeditedJob",
    ],
}

test_module_config_host {
    name: "CtsHostsideNetworkPolicyTests_NoRequiresDevice",
    base: "CtsHostsideNetworkPolicyTests",
    test_suites: ["general-tests"],
    exclude_annotations: [
        "android.platform.test.annotations.RequiresDevice",
        "androidx.test.filters.RequiresDevice",
    ],
}
