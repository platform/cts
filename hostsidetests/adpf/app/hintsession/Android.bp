// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

cc_test_library {
    name: "libadpfhintsession_test_helper_jni",
    min_sdk_version: "34",
    srcs: [
        "src/cpp/**/*.cpp",
        "src/cpp/**/*.c",
    ],
    shared_libs: [
        "libandroid",
        "libjnigraphics",
        "libnativehelper",
        "libnativewindow",
        "libEGL",
        "libGLESv3",
        "liblog",
    ],
    cpp_std: "c++20",
    stl: "c++_shared",
    sdk_version: "current",
}

android_test_helper_app {
    name: "CtsADPFHintSessionDeviceApp",
    min_sdk_version: "34",
    defaults: ["cts_adpf_defaults"],
    srcs: ["src/**/*.java"],
    static_libs: [
        "androidx.test.rules",
        "collector-device-lib",
        "com.google.android.material_material",
    ],
    sdk_version: "current",
    jni_libs: ["libadpfhintsession_test_helper_jni"],
}
