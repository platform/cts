/*
 * Copyright 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cassert>
#include <chrono>
#include <string>

#include "Renderer.h"

class Utility {
public:
    static bool checkAndLogGlError(bool alwaysLog = false);

    static inline void assertGlError() { assert(checkAndLogGlError()); }

    /**
     * Generates an orthographic projection matrix given the half height, aspect ratio, near, and
     * far planes
     *
     * @param outMatrix the matrix to write into
     * @param halfHeight half of the height of the screen
     * @param aspect the width of the screen divided by the height
     * @param near the distance of the near plane
     * @param far the distance of the far plane
     * @return the generated matrix, this will be the same as @a outMatrix so you can chain calls
     *     together if needed
     */
    static float *buildOrthographicMatrix(float *outMatrix, float halfHeight, float aspect,
                                          float near, float far);

    static float *buildIdentityMatrix(float *outMatrix);

    static void setFailure(std::string message, Renderer *renderer = nullptr);

    static int64_t now() {
        return duration_cast<std::chrono::nanoseconds>(
                       std::chrono::steady_clock::now().time_since_epoch())
                .count();
    }

    // Converts lists of numbers into strings, so they can be
    // passed up to the Java code the results map.
    template <typename T>
    static std::string serializeValues(const std::vector<T> &values) {
        std::stringstream stream;
        for (auto &&value : values) {
            stream << value;
            stream << ",";
        }
        std::string out = stream.str();
        out.pop_back(); // remove the last comma
        return out;
    }
};
