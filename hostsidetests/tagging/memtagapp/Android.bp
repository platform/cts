// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_test_helper_app {
    name: "CtsHostsideTaggingMemtagApp",
    defaults: ["cts_tagging_app_defaults"],
    srcs: ["src/**/*.java"],
    test_suites: [
        "general-tests",
    ],
    static_libs: [
        "androidx.test.ext.truth",
        "hamcrest-library",
    ],
    sdk_version: "test_current",
    jni_libs: [
        "libcts_memtagapp_jni",
        "libcts_jni",
    ],
}

cc_library {
    name: "libcts_memtagapp_jni",
    srcs: ["jni/libcts_memtagapp_jni.cpp"],
    header_libs: ["jni_headers"],
    sdk_version: "current",
    stl: "libc++",
    target: {
        // We can't use `sanitize:` here, because Soong does not allow
        // sanitize for sdk_version.
        // TODO(b/377739631): once Soong support this, reconsider this
        // decision. Using cflags and ldflags is closer to what people
        // using the NDK would actually use, so maybe we should still
        // keep it like this.
        android_arm64: {
            cflags: [
                "-fsanitize=memtag",
                "-Xclang -target-feature",
                "-Xclang +mte",
            ],
            ldflags: ["-fsanitize=memtag"],
        },
    },
}
